﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConvolutionMatrixLibrary;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ConvolutionMatrixTest()
        {
            //Assign
            double[,] input = new double[8, 10];
            for (int x = 0; x < input.GetLength(0); x++)
                for (int y = 0; y < input.GetLength(1); y++)
                    input[x, y] = 2;

            SMatrix inputMatrix = new SMatrix(input);

            double[,] kernel = new double[3, 3];
            for (int x = 0; x < kernel.GetLength(0); x++)
                for (int y = 0; y < kernel.GetLength(1); y++)
                    kernel[x, y] = 1;

            SMatrix kernelMatrix = new SMatrix(kernel);

            double[,] expected = new double[input.GetLength(0) - kernel.GetLength(0) + 1,input.GetLength(1) - kernel.GetLength(1) + 1];
            for (int x = 0; x < expected.GetLength(0); x++)
                for (int y = 0; y < expected.GetLength(1); y++)
                    expected[x, y] = 18;

            //Act
            SMatrix actual = inputMatrix * kernelMatrix;

            //Asser
            CollectionAssert.AreEqual(expected, actual.Arr);

        }
    }
}
