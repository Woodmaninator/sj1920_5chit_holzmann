﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConvolutionMatrixLibrary;
using System.Diagnostics;

namespace ConvolutionMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] a = new double[3, 4];
            a[0, 0] = 1;
            a[0, 1] = 2;
            a[0, 2] = 3;
            a[0, 3] = 4;
            a[1, 0] = 5;
            a[1, 1] = 6;
            a[1, 2] = 7;
            a[1, 3] = 8;
            a[2, 0] = 9;
            a[2, 1] = 10;
            a[2, 2] = 11;
            a[2, 3] = 12;

            SMatrix aa = new SMatrix(a);

            double[,] b = new double[2, 2];
            b[0, 0] = 1;
            b[0, 1] = 2;
            b[1, 0] = 3;
            b[1, 1] = 4;

            SMatrix bb = new SMatrix(b);

            SMatrix erg = aa * bb;

            for(int x = 0; x < erg.Arr.GetLength(0); x++)
            {
                for(int y = 0; y < erg.Arr.GetLength(1); y++)
                {
                    Console.Write(erg.Arr[x,y] + "\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("--------------------------------------------------");

            double[,,] rgbArr = new double[2, 2, 3];
            rgbArr[0, 0, 0] = 1;
            rgbArr[0, 0, 1] = 2;
            rgbArr[0, 0, 2] = 3;
            rgbArr[0, 1, 0] = 1;
            rgbArr[0, 1, 1] = 2;
            rgbArr[0, 1, 2] = 3;
            rgbArr[1, 0, 0] = 1;
            rgbArr[1, 0, 1] = 2;
            rgbArr[1, 0, 2] = 3;
            rgbArr[1, 1, 0] = 1;
            rgbArr[1, 1, 1] = 2;
            rgbArr[1, 1, 2] = 3;

            RGBMatrix rgbMatrix = new RGBMatrix(rgbArr);

            SMatrix ergRgb = rgbMatrix * bb;

            for (int x = 0; x < ergRgb.Arr.GetLength(0); x++)
            {
                for (int y = 0; y < ergRgb.Arr.GetLength(1); y++)
                {
                    Console.Write(ergRgb.Arr[x, y] + "\t");
                }
                Console.WriteLine();
            }


            //Performance Test Verschnitt
            Stopwatch sw = new Stopwatch();
            double[,] input = new double[10, 10];
            double[,] kernel = new double[3, 3];

            for (int x = 0; x < input.GetLength(0); x++)
                for (int y = 0; y < input.GetLength(1); y++)
                    input[x, y] = 2;

            for (int x = 0; x < kernel.GetLength(0); x++)
                for (int y = 0; y < kernel.GetLength(1); y++)
                    kernel[x, y] = 1;

            SMatrix inputMatrix = new SMatrix(input);
            SMatrix kernelMatrix = new SMatrix(kernel);

            sw.Start();
            SMatrix ergMatrix = inputMatrix * kernelMatrix;
            sw.Stop();
            Console.WriteLine(sw.Elapsed.ToString());
            sw.Reset();

            input = new double[100, 100];
            for (int x = 0; x < input.GetLength(0); x++)
                for (int y = 0; y < input.GetLength(1); y++)
                    input[x, y] = 2;

            sw.Start();
            ergMatrix = inputMatrix * kernelMatrix;
            sw.Stop();
            Console.WriteLine(sw.Elapsed.ToString());

            input = new double[1000, 1000];
            for (int x = 0; x < input.GetLength(0); x++)
                for (int y = 0; y < input.GetLength(1); y++)
                    input[x, y] = 2;

            sw.Start();
            ergMatrix = inputMatrix * kernelMatrix;
            sw.Stop();
            Console.WriteLine(sw.Elapsed.ToString());

            input = new double[10000, 10000];
            for (int x = 0; x < input.GetLength(0); x++)
                for (int y = 0; y < input.GetLength(1); y++)
                    input[x, y] = 2;

            sw.Start();
            ergMatrix = inputMatrix * kernelMatrix;
            sw.Stop();
            Console.WriteLine(sw.Elapsed.ToString());
        }
    }
}
