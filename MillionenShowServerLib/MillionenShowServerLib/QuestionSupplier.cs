﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenShowServerLib
{
    public class QuestionSupplier
    {
        List<Question> questions;
        Random r;
        public QuestionSupplier()
        {
            r = new Random();
            questions = new List<Question>();
            //Erstellen der Fragen.

            //Auslesen aus der Datei fragen.txt
            StreamReader sr = null;
            try
            {
                sr = new StreamReader("fragen.txt");
                while(sr.Peek() != -1)
                {
                    string input = sr.ReadLine();
                    string[] inputArr = input.Split(';');
                    questions.Add(
                        new Question
                        {
                            Text = inputArr[1],
                            Answer1 = inputArr[2],
                            Answer2 = inputArr[3],
                            Answer3 = inputArr[4],
                            Answer4 = inputArr[5],
                            CorrectAnswer = Int32.Parse(inputArr[6]),
                            Level = Int32.Parse(inputArr[7])
                        });
                }
            }
            catch(Exception ex)
            {
                //Fehler beim Auslesen der Datei: Hardcoded Fragen werden ausgelesen.
                LoadStaticQuestions();
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        private void LoadStaticQuestions()
        {
            //Statische Zuweisung (wird nicht gebraucht)
            questions.Add(new Question { Text = "3 + 3", Answer1 = "5", Answer2 = "7", Answer3 = "6", Answer4 = "1", CorrectAnswer = 3, Level = 1 });
            questions.Add(new Question { Text = "3 + 7", Answer1 = "5", Answer2 = "7", Answer3 = "10", Answer4 = "1", CorrectAnswer = 3, Level = 1 });
            questions.Add(new Question { Text = "3 * 3", Answer1 = "9", Answer2 = "7", Answer3 = "6", Answer4 = "1", CorrectAnswer = 1, Level = 2 });
            questions.Add(new Question { Text = "300 + 3", Answer1 = "512", Answer2 = "744", Answer3 = "12", Answer4 = "303", CorrectAnswer = 4, Level = 2 });
            questions.Add(new Question { Text = "1000-8", Answer1 = "542", Answer2 = "757", Answer3 = "992", Answer4 = "321", CorrectAnswer = 3, Level = 3 });
            questions.Add(new Question { Text = "ln(e)", Answer1 = "5", Answer2 = "1", Answer3 = "5", Answer4 = "Syntax Error", CorrectAnswer = 2, Level = 3 });
        }
        public Question GetLevel1Question()
        {
            return questions.FindAll(x => x.Level == 1)[r.Next(questions.FindAll(x => x.Level == 1).Count)];
        }
        public Question GetLevel2Question()
        {
            return questions.FindAll(x => x.Level == 2)[r.Next(questions.FindAll(x => x.Level == 2).Count)];
        }
        public Question GetLevel3Question()
        {
            return questions.FindAll(x => x.Level == 3)[r.Next(questions.FindAll(x => x.Level == 3).Count)];
        }
    }
}