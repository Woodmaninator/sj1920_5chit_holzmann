﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenShowServerLib
{
    public class Question
    {
        public String Text { get; set; }
        public String Answer1 { get; set; }
        public String Answer2 { get; set; }
        public String Answer3{ get; set; }
        public String Answer4 { get; set; }
        public int CorrectAnswer { get; set; }
        public int Level { get; set; }
    }
}