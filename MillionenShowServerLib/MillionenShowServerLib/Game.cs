﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenShowServerLib
{
    public class Game
    {
        public int GameID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public Question Question1 { get; set; }
        public Question Question2 { get; set; }
        public Question Question3 { get; set; }

        public int Question1Answer { get; set; }
        public int Question2Answer { get; set; }
        public int Question3Answer { get; set; }

        public int CurrentLevel { get; set; }

        public bool Completed { get; set; }

        public Game(int gameId, DateTime startTime, QuestionSupplier questionSupplier)
        {
            this.GameID = gameId;
            this.StartTime = startTime;
            this.Question1 = questionSupplier.GetLevel1Question();
            this.Question2 = questionSupplier.GetLevel2Question();
            this.Question3 = questionSupplier.GetLevel3Question();
            this.CurrentLevel = 1;
            this.Completed = false;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Game #" + this.GameID + "\n");
            sb.Append("Beginn:" + this.StartTime.ToShortTimeString() + "\n");
            if (this.EndTime != null)
                sb.Append("Ende:" + this.StartTime.ToShortTimeString() + "\n");
            return base.ToString();
        }
    }
}