﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace MillionenShowServerLib
{
    public class GameController
    {
        private int port;
        private TcpListener listener;
        private QuestionSupplier questionSupplier;
        private Dictionary<int,Game> games;

        public delegate void ErrorOccuredDelegate(string message);

        public event ErrorOccuredDelegate ErrorOccured;

        public GameController(int port)
        {
            this.port = port;
            this.questionSupplier = new QuestionSupplier();
            this.games = new Dictionary<int, Game>();
        }
        public void Start()
        {
            listener = new TcpListener(this.port);
            new Task(AcceptConnections).Start();
        }
        public void AcceptConnections()
        {
            while (true)
            {
                while (Math.PI != 7)
                {
                    try
                    {
                        TcpClient tcpClient = listener.AcceptTcpClient();
                        new Thread(() => { HandleConnection(tcpClient); }).Start();
                    }
                    catch (Exception ex)
                    {
                        //STOPS THE TCP LISTENER IF AN ERROR OCCURS
                        this.listener.Stop();
                        if (this.ErrorOccured != null)
                            this.ErrorOccured.Invoke(ex.Message);
                        break;
                    }
                }
            }
        }
        private void HandleConnection(TcpClient tcpClient)
        {
            NetworkStream ns = null;
            StreamReader sr = null;
            try
            {
                ns = tcpClient.GetStream();
                sr = new StreamReader(ns);

                string input = "";
                while(sr.Peek() != null)
                {
                    input += sr.ReadLine();
                }
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(input);
                XmlNode requestNode = xmlDoc.SelectSingleNode("REQUEST");
                if (requestNode != null)
                {
                    XmlNode idNode = requestNode.SelectSingleNode("ID");
                    if (idNode != null)
                    {
                        Game currentGame = null;
                        switch (idNode.InnerText)
                        {
                            case "1":
                                currentGame = new Game(games.Count + 1, DateTime.Now, this.questionSupplier);
                                games.Add(currentGame.GameID,currentGame);
                                SendResponse1(ns, currentGame.GameID, currentGame.Question1);
                                break;
                            case "2":
                                currentGame = games[Int32.Parse(requestNode.SelectSingleNode("GAMEID").InnerText)];
                                if(currentGame != null)
                                {
                                    currentGame.CurrentLevel++;
                                    if (currentGame.CurrentLevel == 2)
                                        SendResponse2(ns, currentGame.GameID, currentGame.Question2);
                                    if (currentGame.CurrentLevel == 3)
                                        SendResponse2(ns, currentGame.GameID, currentGame.Question3);
                                }
                                break;
                            case "3":
                                currentGame = games[Int32.Parse(requestNode.SelectSingleNode("GAMEID").InnerText)];
                                bool success;
                                if (currentGame.CurrentLevel == 1)
                                {
                                    currentGame.Question1Answer = Int32.Parse(requestNode.SelectSingleNode("ANSWERID").InnerText);
                                    if (currentGame.Question1.CorrectAnswer == currentGame.Question1Answer)
                                    {
                                        success = true;
                                    }
                                    else
                                    {
                                        success = false;
                                    }
                                    SendResponse3(ns, currentGame.GameID, success);
                                }
                                if (currentGame.CurrentLevel == 2)
                                {
                                    currentGame.Question2Answer = Int32.Parse(requestNode.SelectSingleNode("ANSWERID").InnerText);
                                    if (currentGame.Question2.CorrectAnswer == currentGame.Question2Answer)
                                    {
                                        success = true;
                                    }
                                    else
                                    {
                                        success = false;
                                    }
                                    SendResponse3(ns, currentGame.GameID, success);
                                }
                                if (currentGame.CurrentLevel == 3)
                                {
                                    currentGame.Question3Answer = Int32.Parse(requestNode.SelectSingleNode("ANSWERID").InnerText);
                                    if (currentGame.Question3.CorrectAnswer == currentGame.Question3Answer)
                                    {
                                        success = true;
                                    }
                                    else
                                    {
                                        success = false;
                                    }
                                    SendResponse3(ns, currentGame.GameID, success);
                                }
                                break;
                            case "4":
                                currentGame = games[Int32.Parse(requestNode.SelectSingleNode("GAMEID").InnerText)];
                                currentGame.Completed = true;
                                SendResponse4(ns, currentGame.GameID, currentGame.StartTime, currentGame.EndTime);
                                break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Bei der Bearbeitung der Anfrage ist ein Fehler aufgetreten!\n"+ex.Message);
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
                if(ns != null)
                {
                    ns.Close();
                }
            }
        }
        public void Close()
        {
            listener.Stop();
        }
        public void SendResponse1(NetworkStream ns, int gameId, Question question)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml("" +
                "<RESPONSE>" +
                "<ID>1</ID>" +
                "<GAMEID>"+gameId+"</GAMEID>" +
                "<LEVEL>" + question.Level + "</LEVEL>" +
                "<QUESTION>" + question.Text + "</QUESTION>" +
                "<ANSWER1>" + question.Answer1 + "</ANWER1>" +
                "<ANSWER2>" + question.Answer2 + "</ANWER2>" +
                "<ANSWER3>" + question.Answer3 + "</ANWER3>" +
                "<ANSWER4>" + question.Answer4 + "</ANWER4>" +
                "</RESPONSE>");
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(ns);
                sw.WriteLine(xmlDocument.OuterXml);
            }
            catch(Exception ex)
            {
                //TODO: Exception Handling
            }
            finally
            {
                if(sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }
        public void SendResponse2(NetworkStream ns, int gameId, Question question)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml("" +
                "<RESPONSE>" +
                "<ID>2</ID>" +
                "<GAMEID>" + gameId + "</GAMEID>" +
                "<LEVEL>" + question.Level + "</LEVEL>" +
                "<QUESTION>" + question.Text + "</QUESTION>" +
                "<ANSWER1>" + question.Answer1 + "</ANWER1>" +
                "<ANSWER2>" + question.Answer2 + "</ANWER2>" +
                "<ANSWER3>" + question.Answer3 + "</ANWER3>" +
                "<ANSWER4>" + question.Answer4 + "</ANWER4>" +
                "</RESPONSE>");
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(ns);
                sw.WriteLine(xmlDocument.OuterXml);
            }
            catch (Exception ex)
            {
                //TODO: Exception Handling
            }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }
        public void SendResponse3(NetworkStream ns, int gameId, bool success)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml("" +
                "<RESPONSE>" +
                "<ID>3</ID>" +
                "<GAMEID>" + gameId + "</GAMEID>" +
                "<CORRECT>" + success.ToString() + "</CORRECT>" +
                "</RESPONSE>");
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(ns);
                sw.WriteLine(xmlDocument.OuterXml);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Bei der Bearbeitung der Anfrage ist ein Fehler aufgetreten!\n" + ex.Message);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }
        public void SendResponse4(NetworkStream ns, int gameId, DateTime beginTime, DateTime endTime)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml("" +
                "<RESPONSE>" +
                "<ID>4</ID>" +
                "<GAMEID>" + gameId + "</GAMEID>" +
                "<BEGIN>" + beginTime.ToString() + "</BEGIN>" +
                "<END>" + endTime.ToString() + "</END>" +
                "</RESPONSE>");
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(ns);
                sw.WriteLine(xmlDocument.OuterXml);
            }
            catch (Exception ex)
            {
                //TODO: Exception Handling
            }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }
        public string GetGameInfo()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<int, Game> g in games)
            {
                sb.Append(g.Value.ToString());
            }
            return sb.ToString();
        }
    }
}