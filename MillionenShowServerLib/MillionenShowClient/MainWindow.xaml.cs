﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Xml;

namespace MillionenShowClient
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TcpClient tcpClient;
        int currentGameID;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ButtonClick(int id)
        {
            try
            {
                new Thread(() =>
                {
                    tcpClient = new TcpClient("127.0.0.1", 3333);
                    NetworkStream ns = tcpClient.GetStream();
                    StreamWriter sw = new StreamWriter(ns);
                    //Schreiben des Requests
                    sw.WriteLine("" +
                        "<REQUEST>" +
                        "<ID>3</ID>" +
                        "<GAMEID>" + this.currentGameID + "</GAMEID>" +
                        "<ANSWERID>" + id + "</ANSWERID>" +
                        "</REQUEST>");
                    StreamReader sr = new StreamReader(ns);
                    sw.AutoFlush = true;
                    string input = "";
                    while (input == "")
                    {
                        while (sr.Peek() != -1)
                        {
                            input += sr.ReadLine();
                        }
                    }
                    //Interpretation des Inputs
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(input);
                    if (Boolean.Parse(xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("CORRECT").InnerText))
                    {
                        sw.WriteLine("" +
                            "<REQUEST>" +
                            "<ID>2</ID>" +
                            "<GAMEID>" + this.currentGameID + "</GAMEID>" +
                            "<ANSWERID></ANSWERID>" +
                            "</REQUEST>");

                        input = "";
                        while (input == "")
                        {
                            while (sr.Peek() != -1)
                            {
                                input += sr.ReadLine();
                            }
                        }
                        xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(input);
                        this.labelGameID.Dispatcher.Invoke(() => this.labelGameID.Content = "GameID: " + this.currentGameID);
                        this.labelCurrentLevel.Dispatcher.Invoke(() => this.labelCurrentLevel.Content = "Current Level: " + xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("LEVEL").InnerText);
                        this.buttonA1.Dispatcher.Invoke(() => this.buttonA1.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER1").InnerText);
                        this.buttonA2.Dispatcher.Invoke(() => this.buttonA2.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER2").InnerText);
                        this.buttonA3.Dispatcher.Invoke(() => this.buttonA3.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER3").InnerText);
                        this.buttonA4.Dispatcher.Invoke(() => this.buttonA4.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER4").InnerText);

                        this.buttonA1.Dispatcher.Invoke(() => this.buttonA1.IsEnabled = true);
                        this.buttonA2.Dispatcher.Invoke(() => this.buttonA2.IsEnabled = true);
                        this.buttonA3.Dispatcher.Invoke(() => this.buttonA3.IsEnabled = true);
                        this.buttonA4.Dispatcher.Invoke(() => this.buttonA4.IsEnabled = true);
                    }
                    else
                    {
                        sw.WriteLine("" +
                            "<REQUEST>" +
                            "<ID>4</ID>" +
                            "<GAMEID>" + this.currentGameID + "</GAMEID>" +
                            "<ANSWERID></ANWERID>" +
                            "</REQUEST>");
                        input = "";
                        while (input == "")
                        {
                            while (sr.Peek() != -1)
                            {
                                input += sr.ReadLine();
                            }
                        }
                        xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(input);
                        this.Dispatcher.Invoke(() =>
                        {
                            MessageBox.Show("Game Over!\nStart-Time: " + DateTime.Parse(xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("BEGIN").InnerText).ToShortTimeString()
                                + "\nEnd-Time: " + DateTime.Parse(xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("END").InnerText).ToShortTimeString());
                        });
                        this.labelGameID.Dispatcher.Invoke(() => this.labelGameID.Content = "GameID: ");
                        this.labelCurrentLevel.Dispatcher.Invoke(() => this.labelCurrentLevel.Content = "Current Level: ");
                        this.buttonA1.Dispatcher.Invoke(() => this.buttonA1.IsEnabled = false);
                        this.buttonA2.Dispatcher.Invoke(() => this.buttonA2.IsEnabled = false);
                        this.buttonA3.Dispatcher.Invoke(() => this.buttonA3.IsEnabled = false);
                        this.buttonA4.Dispatcher.Invoke(() => this.buttonA4.IsEnabled = false);
                    }
                    sw.Close();
                    sr.Close();
                    ns.Close();
                    tcpClient.Close();
                }).Start();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Bei der Anfrage an den Server ist ein Fehler aufgetreten.\n" + ex.Message);
            }
        }
        private void buttonA1_Click(object sender, RoutedEventArgs e)
        {
            ButtonClick(1);
        }

        private void buttonA2_Click(object sender, RoutedEventArgs e)
        {
            ButtonClick(2);
        }

        private void buttonA3_Click(object sender, RoutedEventArgs e)
        {
            ButtonClick(3);
        }

        private void buttonA4_Click(object sender, RoutedEventArgs e)
        {
            ButtonClick(4);
        }

        private void labelNewGame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                new Thread(() =>
                {
                    tcpClient = new TcpClient("127.0.0.1", 3333);
                    NetworkStream ns = tcpClient.GetStream();
                    StreamWriter sw = new StreamWriter(ns);
                    //Schreiben des Requests
                    sw.WriteLine("" +
                        "<REQUEST>" +
                        "<ID>1</ID>" +
                        "<GAMEID></GAMEID>" +
                        "<ANSWERID></ANSWERID>" +
                        "</REQUEST>");
                    StreamReader sr = new StreamReader(ns);
                    sw.AutoFlush = true;
                    string input = "";
                    while (input == "")
                    {
                        while (sr.Peek() != -1)
                        {
                            input += sr.ReadLine();
                        }
                    }
                    //Interpretation des Inputs
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(input);
                    this.currentGameID = Int32.Parse(xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("GAMEID").InnerText);

                    this.labelGameID.Dispatcher.Invoke(() => this.labelGameID.Content = "GameID: " + this.currentGameID);
                    this.labelCurrentLevel.Dispatcher.Invoke(() => this.labelCurrentLevel.Content = "Current Level: 1");
                    this.buttonA1.Dispatcher.Invoke(() => this.buttonA1.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER1").InnerText);
                    this.buttonA2.Dispatcher.Invoke(() => this.buttonA2.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER2").InnerText);
                    this.buttonA3.Dispatcher.Invoke(() => this.buttonA3.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER3").InnerText);
                    this.buttonA4.Dispatcher.Invoke(() => this.buttonA4.Content = xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("ANSWER4").InnerText);

                    this.buttonA1.Dispatcher.Invoke(() => this.buttonA1.IsEnabled = true);
                    this.buttonA2.Dispatcher.Invoke(() => this.buttonA2.IsEnabled = true);
                    this.buttonA3.Dispatcher.Invoke(() => this.buttonA3.IsEnabled = true);
                    this.buttonA4.Dispatcher.Invoke(() => this.buttonA4.IsEnabled = true);
                    sw.Close();
                    sr.Close();
                    ns.Close();
                    tcpClient.Close();
                }).Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bei der Anfrage an den Server ist ein Fehler aufgetreten.\n" + ex.Message);
            }
        }

        private void labelEndGame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                new Thread(() =>
                {
                    tcpClient = new TcpClient("127.0.0.1", 3333);
                    NetworkStream ns = tcpClient.GetStream();
                    StreamWriter sw = new StreamWriter(ns);
                    //Schreiben des Requests
                    sw.WriteLine("" +
                        "<REQUEST>" +
                        "<ID>4</ID>" +
                        "<GAMEID>" + this.currentGameID + "</GAMEID>" +
                        "<ANSWERID></ANSWERID>" +
                        "</REQUEST>");
                    StreamReader sr = new StreamReader(ns);
                    sw.AutoFlush = true;
                    string input = "";
                    while (input == "")
                    {
                        while (sr.Peek() != -1)
                        {
                            input += sr.ReadLine();
                        }
                    }
                    //Interpretation des Inputs
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(input);
                    this.currentGameID = Int32.Parse(xmlDocument.SelectSingleNode("RESPONSE").SelectSingleNode("GAMEID").InnerText);
                    sw.Close();
                    sr.Close();
                    ns.Close();
                    tcpClient.Close();
                }).Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bei der Anfrage an den Server ist ein Fehler aufgetreten.\n" + ex.Message);
            }
        }
    }
}