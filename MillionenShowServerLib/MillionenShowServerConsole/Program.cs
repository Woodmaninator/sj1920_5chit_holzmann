﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MillionenShowServerLib;

namespace MillionenShowServerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            GameController controller = new GameController(3333);
            controller.Start();
            bool running = true;
            try 
            {
                Console.WriteLine("E = Beenden");
                Console.WriteLine("1 = Statistik");
                while (running)
                {
                    switch (Console.ReadLine())
                    {
                        case "E":
                            controller.Close();
                            running = false;
                            break;
                        case "1":
                            Console.WriteLine(controller.GetGameInfo());
                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Es ist ein Fehler aufgetreten:\n" + ex.Message);
            }
        }
    }
}
