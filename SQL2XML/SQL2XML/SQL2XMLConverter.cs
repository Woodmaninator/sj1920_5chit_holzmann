﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SQL2XML
{
    class SQL2XMLConverter
    {
        List<Patient> patients;
        List<Behandlung> behandlungen;
        List<PatientBehandlung> patientenBehandlungen;
        public SQL2XMLConverter(string path)
        {
            patients = new List<Patient>();
            behandlungen = new List<Behandlung>();
            patientenBehandlungen = new List<PatientBehandlung>();
            OleDbConnection con = new OleDbConnection("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + path + "; Persist Security Info = False;");
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT ID, Nachname, Vorname, SVNR FROM Patient;";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Patient patient = new Patient();
                patient.ID = reader.GetInt32(0);
                patient.Nachname = reader.GetString(1);
                patient.Vorname = reader.GetString(2);
                patient.SVNR = reader.GetString(3);
                patients.Add(patient);
            }
            reader.Close();
            cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT ID, Bezeichnung FROM Behandlung";
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Behandlung behandlung = new Behandlung();
                behandlung.ID = reader.GetInt32(0);
                behandlung.Bezeichnung = reader.GetString(1);
                behandlungen.Add(behandlung);

            }
            reader.Close();
            cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT PatientID, BehandlungsID, Dauer, Datum FROM PatientBehandlungen";
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                PatientBehandlung pb = new PatientBehandlung();
                pb.Patient = patients.Find(x => x.ID == reader.GetInt32(0));
                pb.Behandlung = behandlungen.Find(x => x.ID == reader.GetInt32(1));
                pb.Dauer = reader.GetInt32(2);
                pb.Datum = reader.GetDateTime(3);
                patientenBehandlungen.Add(pb);
            }
            con.Close();
        }
        public void ConvertAllTablesToXml(string path)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml("<Patients></Patients>");

            //XmlDeclaration declarationNode = (XmlDeclaration)document.CreateNode(XmlNodeType.XmlDeclaration, "xml", string.Empty);
            //declarationNode.Encoding = "UTF-16";
            //document.InsertBefore(declarationNode, document.FirstChild);

            foreach (Patient item in patients)
            {
                XmlNode patient = document.CreateElement("Patient");

                XmlAttribute id = document.CreateAttribute("ID");
                id.Value = item.ID.ToString();

                XmlNode vorname = document.CreateElement("Vorname");
                vorname.InnerText = item.Vorname;

                XmlNode nachname = document.CreateElement("Nachname");
                nachname.InnerText = item.Nachname;

                XmlNode svnr = document.CreateElement("SVNR");
                svnr.InnerText = item.SVNR;



                patient.Attributes.Append(id);
                patient.AppendChild(vorname);
                patient.AppendChild(nachname);
                patient.AppendChild(svnr);

                XmlNode r = document.SelectSingleNode("Patients");
                r.AppendChild(patient);
            }
            foreach (PatientBehandlung item in patientenBehandlungen)
            {
                XmlNode patient = document.SelectSingleNode("/Patients/Patient[@ID='" + item.Patient.ID + "']");

                XmlNode behandlung = document.CreateElement("Behandlung");

                XmlAttribute id = document.CreateAttribute("ID");
                id.Value = item.Behandlung.ID.ToString();

                XmlNode bezeichnung = document.CreateElement("Bezeichnung");
                bezeichnung.InnerText = item.Behandlung.Bezeichnung;

                XmlNode dauer = document.CreateElement("Dauer");
                dauer.InnerText = item.Dauer.ToString();

                XmlNode datum = document.CreateElement("Datum");
                datum.InnerText = item.Datum.ToString().Substring(0, 10);

                behandlung.Attributes.Append(id);
                behandlung.AppendChild(bezeichnung);
                behandlung.AppendChild(dauer);
                behandlung.AppendChild(datum);

                patient.AppendChild(behandlung);
            }
            document.Save(path);
        }
        public KeyValues GetKeyValuesSQL()
        {
            int sum = 0;
            foreach (PatientBehandlung pb in patientenBehandlungen)
                sum += pb.Dauer;
            return new KeyValues
            {
                PatientCount = patients.Count,
                BehandlungCount = behandlungen.Count,
                PatientBehandlungCount = patientenBehandlungen.Count,
                PatientenWithOneBehandlungCount = patients.FindAll(x =>
                {
                    return patientenBehandlungen.FindAll(y => y.Patient.ID == x.ID).Count == 1;
                }).Count,
                PatientenWithTwoBehandlungenCount = patients.FindAll(x =>
                {
                    return patientenBehandlungen.FindAll(y => y.Patient.ID == x.ID).Count == 2;
                }).Count,
                PatientenWithMoreThanTwoBehandlungenCount = patients.FindAll(x =>
                {
                    return patientenBehandlungen.FindAll(y => y.Patient.ID == x.ID).Count > 2;
                }).Count,
                BehandlungDurationSum = sum
            };
        }
        public KeyValues GetKeyValuesXML(string path)
        {
            List<Patient> patienten = new List<Patient>();
            List<Behandlung> behandlungen = new List<Behandlung>();
            List<PatientBehandlung> patientBehandlungen = new List<PatientBehandlung>();
            LoadXMLFile(path, patienten, behandlungen, patientBehandlungen);
            int sum = 0;
            foreach (PatientBehandlung pb in patientBehandlungen)
                sum += pb.Dauer;
            return new KeyValues
            {
                PatientCount = patienten.Count,
                BehandlungCount = behandlungen.Count,
                PatientBehandlungCount = patientBehandlungen.Count,
                PatientenWithOneBehandlungCount = patienten.FindAll(x =>
                {
                    return patientBehandlungen.FindAll(y => y.Patient.ID == x.ID).Count == 1;
                }).Count,
                PatientenWithTwoBehandlungenCount = patienten.FindAll(x =>
                {
                    return patientBehandlungen.FindAll(y => y.Patient.ID == x.ID).Count == 2;
                }).Count,
                PatientenWithMoreThanTwoBehandlungenCount = patienten.FindAll(x =>
                {
                    return patientBehandlungen.FindAll(y => y.Patient.ID == x.ID).Count > 2;
                }).Count,
                BehandlungDurationSum = sum
            };
        }
        private void LoadXMLFile(string xmlFileName, List<Patient> patienten, List<Behandlung> behandlungen, List<PatientBehandlung> patientBehandlungen)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFileName);

            XmlNodeList patientenNodes = doc.SelectNodes("/Patients/Patient");

            foreach (XmlNode patientNode in patientenNodes)
            {
                int id = Convert.ToInt32(patientNode.Attributes["ID"].Value);
                string vorname = patientNode.SelectSingleNode("Vorname").InnerText;
                string nachname = patientNode.SelectSingleNode("Nachname").InnerText;
                string svnr = patientNode.SelectSingleNode("SVNR").InnerText;

                Patient patient = new Patient { ID = id, Vorname = vorname, Nachname = nachname, SVNR = svnr };

                XmlNodeList behandlungenNodes = patientNode.SelectNodes("Behandlung");

                foreach (XmlNode behandlungNode in behandlungenNodes)
                {
                    if (!behandlungen.Exists(x => x.Bezeichnung == behandlungNode.SelectSingleNode("Bezeichnung").InnerText))
                    {
                        int bId = Convert.ToInt32(behandlungNode.Attributes["ID"].Value);
                        string bName = behandlungNode.SelectSingleNode("Bezeichnung").InnerText;
                        Behandlung b = new Behandlung { ID = bId, Bezeichnung = bName };
                        behandlungen.Add(b);
                    }

                    Behandlung currentBehandlung = behandlungen.Find(x => x.Bezeichnung == behandlungNode.SelectSingleNode("Bezeichnung").InnerText);

                    DateTime date = DateTime.Parse(behandlungNode.SelectSingleNode("Datum").InnerText);
                    int duration = Int32.Parse(behandlungNode.SelectSingleNode("Dauer").InnerText);

                    PatientBehandlung patientBehandlung = new PatientBehandlung { Behandlung = currentBehandlung, Patient = patient, Datum = date, Dauer = duration };
                    patientBehandlungen.Add(patientBehandlung);
                }

                patienten.Add(patient);
            }
        }
    }
}

