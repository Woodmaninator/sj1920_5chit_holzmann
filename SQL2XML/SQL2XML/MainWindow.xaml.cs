﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SQL2XML
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SQL2XMLConverter converter;
        private KeyValues keyValuesSQL;
        private KeyValues keyValuesXML;
        public MainWindow()
        {
            InitializeComponent();
            convertButton.IsEnabled = false;
            
        }

        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Access Database (*.accdb) | *accdb";
            if (dialog.ShowDialog() == true)
            {
                try
                {
                    converter = new SQL2XMLConverter(dialog.FileName);
                    keyValuesSQL = converter.GetKeyValuesSQL();
                    convertButton.IsEnabled = true;

                }
                catch(Exception ex)
                {
                    MessageBox.Show("An error occured!\n" + ex.Message);
                    convertButton.IsEnabled = false;
                }
            }
        }

        private void ConvertButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML-File (*.xml) | *.xml";
            saveFileDialog.AddExtension = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    converter.ConvertAllTablesToXml(saveFileDialog.FileName);
                    keyValuesXML = converter.GetKeyValuesXML(saveFileDialog.FileName);
                    ShowSuccess();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured while creating the XML-File!\n" + ex.Message);
                }
            }
        }
        private void ShowSuccess()
        {
            //Load SQL Key Values
            labelSQL1.Text = keyValuesSQL.PatientCount.ToString();
            labelSQL2.Text = keyValuesSQL.PatientBehandlungCount.ToString();
            labelSQL3.Text = keyValuesSQL.BehandlungCount.ToString();
            labelSQL4.Text = keyValuesSQL.PatientenWithOneBehandlungCount.ToString();
            labelSQL5.Text = keyValuesSQL.PatientenWithTwoBehandlungenCount.ToString();
            labelSQL6.Text = keyValuesSQL.PatientenWithMoreThanTwoBehandlungenCount.ToString();
            labelSQL7.Text = keyValuesSQL.BehandlungDurationSum.ToString();

            //Load XML Key Values
            labelXML1.Text = keyValuesXML.PatientCount.ToString();
            labelXML2.Text = keyValuesXML.PatientBehandlungCount.ToString();
            labelXML3.Text = keyValuesXML.BehandlungCount.ToString();
            labelXML4.Text = keyValuesXML.PatientenWithOneBehandlungCount.ToString();
            labelXML5.Text = keyValuesXML.PatientenWithTwoBehandlungenCount.ToString();
            labelXML6.Text = keyValuesXML.PatientenWithMoreThanTwoBehandlungenCount.ToString();
            labelXML7.Text = keyValuesXML.BehandlungDurationSum.ToString();

            if (keyValuesSQL.Equals(keyValuesXML))
                MessageBox.Show("Success");
            else
                MessageBox.Show("Not success");
        }
    }
}
