﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL2XML
{
    class KeyValues:IEquatable<KeyValues>
    {
        public int PatientCount { get; set; }
        public int PatientBehandlungCount { get; set; }
        public int BehandlungCount { get; set; }
        public int PatientenWithOneBehandlungCount { get; set; }
        public int PatientenWithTwoBehandlungenCount { get; set; }
        public int PatientenWithMoreThanTwoBehandlungenCount { get; set; }
        public int BehandlungDurationSum { get; set; }

        public bool Equals(KeyValues other)
        {
            if (PatientCount != other.PatientCount)
                return false;
            if (BehandlungCount != other.BehandlungCount)
                return false;
            if (PatientBehandlungCount != other.PatientBehandlungCount)
                return false;
            if (PatientenWithOneBehandlungCount != other.PatientenWithOneBehandlungCount)
                return false;
            if (PatientenWithTwoBehandlungenCount != other.PatientenWithTwoBehandlungenCount)
                return false;
            if (PatientenWithMoreThanTwoBehandlungenCount != other.PatientenWithMoreThanTwoBehandlungenCount)
                return false;
            if (BehandlungDurationSum != other.BehandlungDurationSum)
                return false;
            return true;
        }
    }
}
