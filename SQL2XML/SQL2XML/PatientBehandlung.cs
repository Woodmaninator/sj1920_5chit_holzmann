﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL2XML
{
    class PatientBehandlung
    {
        public int Dauer { get; set; }
        public DateTime Datum { get; set; }
        public Patient Patient { get; set; }
        public Behandlung Behandlung { get; set; }
    }
}
