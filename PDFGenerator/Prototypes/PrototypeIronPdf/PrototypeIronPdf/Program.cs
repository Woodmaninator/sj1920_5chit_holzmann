﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPdf;

namespace PrototypeIronPdf
{
    class Program
    {
        static void Main(string[] args)
        {
            HtmlToPdf pdfGenerator = new HtmlToPdf();
            pdfGenerator.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            pdfGenerator.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Portrait;

            PdfDocument doc = pdfGenerator.RenderHtmlAsPdf("<h1>Es geht!</h1 id=\"here\"><form>First name:<br> <input type = 'text' name = 'firstname' value = ''><br>Last name:<br> <input type = 'text' name = 'lastname' value=''></form><p>Parapgraph Text</p><table border=1><tr><td>A</td><td>B</td></tr><tr><td>A</td><td>B</td></tr></table><a href=\"http://www.google.at\">Click me!</a><a href=\"#here\">Click me!</a>");
           
            doc.SaveAs("pdf.pdf");
        }
    }
}
