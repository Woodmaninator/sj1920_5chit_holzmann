﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;

namespace PrototypePDFSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Document doc = new Document();

            Section section = doc.AddSection();

            //TEXT FORMATTING & LAYOUT
            section.AddParagraph("Beispieltext");
            section.AddParagraph("Beispieltext");
            section.AddParagraph();
            section.AddParagraph("Beispieltext");

            Paragraph p = section.AddParagraph();
            p.Format.Font.Color = Colors.Red;
            p.Format.Font.Size = 12;
            FormattedText ft = p.AddFormattedText("Ich bin ein Text mit Ümläuten", TextFormat.Bold);
            ft.FontName = "Comic Sans MS";

            //LAYOUT
            p.Format.Alignment = ParagraphAlignment.Center;
            p.Format.SpaceAfter = 4;
            p.Format.SpaceBefore = 10;

            //FOOTER & HEADER
            Paragraph footerParagraph = section.Footers.Primary.AddParagraph();
            footerParagraph.AddFormattedText("I am a footer!", TextFormat.Italic);
            footerParagraph.Format.Alignment = ParagraphAlignment.Center;

            Paragraph headerParagraph = section.Headers.Primary.AddParagraph();
            headerParagraph.AddFormattedText("I am a header!", TextFormat.Italic);

            //TABLE
            Table table = section.AddTable();
            table.Style = "Table";
            table.Borders.Color = Colors.Aqua;
            table.Borders.Width = 0.25;

            Column col = table.AddColumn();
            col.Format.Alignment = ParagraphAlignment.Center;
            col = table.AddColumn();
            col.Format.Alignment = ParagraphAlignment.Center;
            col = table.AddColumn();
            col.Format.Alignment = ParagraphAlignment.Center;
            col = table.AddColumn();
            col.Format.Alignment = ParagraphAlignment.Center;
            col = table.AddColumn();
            col.Format.Alignment = ParagraphAlignment.Center;

            Row headerRow = table.AddRow();
            Shading s = new Shading();
            s.Color = Colors.Aqua;
            headerRow.Shading = s;
            headerRow.HeadingFormat = true;
   
            headerRow.Cells[0].AddParagraph("Header 1");
            headerRow.Cells[1].AddParagraph("Header 2");
            headerRow.Cells[2].AddParagraph("Header 3");
            headerRow.Cells[3].AddParagraph("Header 4");
            headerRow.Cells[4].AddParagraph("Header 5");

            //Filling the Table
            for(int i = 0; i < 20; i++)
            {
                Row row = table.AddRow();
                for(int j = 0; j < 5; j++)
                {
                    row.Cells[j].AddParagraph("Cell " + (j + 1));
                }
            }

            //Using Images
            Image image = section.AddImage("image.png");
            image.Width = 400;
            image.Height = 200;

            //Creating Links
            Paragraph linkParagraph = section.AddParagraph();
            Hyperlink h = linkParagraph.AddHyperlink("http://stackoverflow.com/", HyperlinkType.Web);
            h.AddFormattedText("http://www.stackoverflow.com/");

            //Creating the PDF
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always);
            pdfRenderer.Document = doc;
            pdfRenderer.RenderDocument();

            pdfRenderer.PdfDocument.Save("asdf.pdf");

            //Starting the PDF
            Process.Start("asdf.pdf");
        }
    }
}
