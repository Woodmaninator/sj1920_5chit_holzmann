﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PDFGenerator;

namespace PDFGeneratorWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Controller controller;
        public MainWindow()
        {
            InitializeComponent();

            controller = new Controller();
            foreach(Bill b in controller.Bills)
            {
                listBox.Items.Add(b.Title);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if((string)listBox.SelectedItem != "" && path.Text != "" && path.Text.Contains(".pdf"))
            {
                try
                {
                    controller.PrintPDF(controller.Bills.Find(x => (string)listBox.SelectedItem == x.Title), "pdf.pdf");
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Bei der Erstellung des Dokuments ist ein Fehler aufgetreten!\n" + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Bitte eine Rechnung auswählen und einen gültigen Pfad angeben. Der Pfad muss mit \".pdf\" enden");
            }
        }
    }
}
