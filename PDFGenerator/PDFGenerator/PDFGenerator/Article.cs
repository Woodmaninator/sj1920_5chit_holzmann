﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFGenerator
{
    public class Article
    {
        public int ID
        {
            get;set;
        }
        public string Name
        {
            get;set;
        }

        public int Amount
        {
            get;set;
        }

        public double Discount
        {
            get;set;
        }

        public double Price
        {
            get;set;
        }

        public double Tax
        {
            get;set;
        }

        public string Unit
        {
            get;set;
        }
    }
}
