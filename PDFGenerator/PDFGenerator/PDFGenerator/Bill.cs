﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFGenerator
{
    public class Bill
    {
        public int ID { get; set; }
        public string Title
        {
            get;set;
        }

        public string ProjectText
        {
            get;set;
        }

        public DateTime Date
        {
            get;set;
        }

        public string Clerk
        {
            get;set;
        }

        public string FooterText
        {
            get;set;
        }

        public string IntroText
        {
            get;set;
        }

        public double Discount
        {
            get;set;
        }

        public string EndText
        {
            get;set;
        }

        public List<Article> Articles
        {
            get;set;
        }

        public Customer Customer
        {
            get;set;
        }

        public Company Company
        {
            get;set;
        }

        public double CashDiscount
        {
            get;set;
        }

        public int CashLimit
        {
            get;set;
        }

        public int CashDiscountLimit
        {
            get;set;
        }
    }
}
