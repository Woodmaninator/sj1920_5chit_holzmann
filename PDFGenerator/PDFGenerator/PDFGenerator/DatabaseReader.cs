﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PDFGenerator
{
    public class DatabaseReader
    {
        private string connectionString = "Server=localhost;Database=pdfRechnung;Uid=root;Pwd=;";

        public List<Bill> ReadBillsFromDatabase()
        {
            List<Bill> bills = new List<Bill>();
            MySqlConnection connection = null;
            MySqlDataReader reader = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand command = new MySqlCommand();
                command.Connection = connection;
                command.CommandText = 
                    "SELECT r.ID, r.Titel, r.Projekttext, r.Datum, r.Sachbearbeiter, r.FooterText, r.IntroText, r.Rabatt, r.EndText, " +
                    "f.ID, f.Bezeichnung, f.Strasse, f.Ort, f.PLZ, " +
                    "k.ID, k.Vorname, k.Nachname, k.Strasse, k.Ort, k.PLZ, " +
                    "s.ID, s.Prozentsatz, s.Tage, s.Zahlungsziel " +
                    "FROM rechnung r INNER JOIN firma f ON r.Firma_ID = f.ID " +
                    "INNER JOIN kunde k ON r.Kunde_ID = k.ID " +
                    "INNER JOIN skonto s ON r.Skonto_ID = s.ID ";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Bill bill = new Bill
                    {
                        ID = reader.GetInt32(0),
                        Title = reader.GetString(1),
                        ProjectText = reader.GetString(2),
                        Date = reader.GetDateTime(3),
                        Clerk = reader.GetString(4),
                        FooterText = reader.GetString(5),
                        IntroText = reader.GetString(6),
                        Discount = reader.GetDouble(7),
                        EndText = reader.GetString(8),
                        Company = new Company
                        {
                            ID = reader.GetInt32(9),
                            Name = reader.GetString(10),
                            Street = reader.GetString(11),
                            Place = reader.GetString(12),
                            PostCode = reader.GetInt32(13)
                        },
                        Customer = new Customer
                        {
                            ID = reader.GetInt32(14),
                            FirstName = reader.GetString(15),
                            LastName = reader.GetString(16),
                            Street = reader.GetString(17),
                            Place = reader.GetString(18),
                            PostCode = reader.GetInt32(19)
                        },
                        CashDiscount = reader.GetDouble(21),
                        CashDiscountLimit = reader.GetInt32(22),
                        CashLimit = reader.GetInt32(23),
                        Articles = new List<Article>()
                    };
                    bills.Add(bill);
                }
                reader.Close();
                command = new MySqlCommand();
                command.Connection = connection;
                command.CommandText =
                    "SELECT Rechnung_ID, ID, Bezeichnung, Einheit, Anzahl, Rabatt, Einzelpreis, UST " +
                    "FROM artikel";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Article article = new Article
                    {
                        ID = reader.GetInt32(1),
                        Name = reader.GetString(2),
                        Unit = reader.GetString(3),
                        Amount = reader.GetInt32(4),
                        Discount = reader.GetDouble(5),
                        Price = reader.GetDouble(6),
                        Tax = reader.GetDouble(7)
                    };
                    bills.Find(x => x.ID == reader.GetInt32(0))?.Articles.Add(article);
                }
            } 
            catch
            {
                throw;
            }
            finally
            {
                if (connection?.State == System.Data.ConnectionState.Open)
                    connection.Close();
                if (reader?.IsClosed == false)
                    reader.Close();
            }
            return bills;
        }
    }
}