﻿using IronPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PDFGenerator
{
    public class PDFPrinter
    {
        public void Print(Bill bill, string path)
        {
            //Temporary File
            path = "pdf.pdf";
            StringBuilder htmlBuilder = new StringBuilder();
            //Beginning of the document
            htmlBuilder.Append("<html>");
            
            //Head of the Document
            htmlBuilder.Append("<head>");

            //Fucking Bootstrap
            htmlBuilder.Append("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">");
            htmlBuilder.Append("<script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>");
            htmlBuilder.Append("<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>");
            htmlBuilder.Append("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>");

            //Style Shit
            htmlBuilder.Append(
                "<style>" +
                "p{font-size: 16px;margin: 0px;}" +
                "p#companyText{font-size: 24px;font-weight: bold;}" +
                "p#projectTitle{font-size: 30px;font-weight: bold;}" +
                "</style>"
                );

            //End of the Head
            htmlBuilder.Append("</head>");

            //Body of the Document
            htmlBuilder.Append("<body>");
            htmlBuilder.Append("<div class=container>");

            //Company
            htmlBuilder.Append("<div>");
            htmlBuilder.Append("<p class=\"text-right\" id=\"companyText\">" + bill.Company.Name + "</p>");
            htmlBuilder.Append("<p class=\"text-right\">" + bill.Company.Street+ "</p>");
            htmlBuilder.Append("<p class=\"text-right\">" + bill.Company.PostCode + " " + bill.Company.Place + "</p>");
            htmlBuilder.Append("<hr>");
            htmlBuilder.Append("</div>");
            htmlBuilder.Append("<br>");
            //Customer
            htmlBuilder.Append("<div>");
            htmlBuilder.Append("<p>" + bill.Customer.LastName + "</p>");
            htmlBuilder.Append("<p>" + bill.Customer.FirstName + "</p>");
            htmlBuilder.Append("<p>" + bill.Customer.Street + "</p>");
            htmlBuilder.Append("<p>" + bill.Customer.PostCode + " " + bill.Customer.Place + "</p>");
            htmlBuilder.Append("</div>");

            //Date / Clerk
            htmlBuilder.Append("<div>");
            htmlBuilder.Append("<p class=\"text-right\">" + bill.Date.ToShortDateString() + "</p>");
            htmlBuilder.Append("<p class=\"text-right\">Sachbearbeiter: " + bill.Clerk + "</p>");
            htmlBuilder.Append("</div>");

            //Titel von da Rechnung und so ein Shit hoid
            htmlBuilder.Append("<div>");
            htmlBuilder.Append("<p id=\"projectTitle\">" + bill.Title + "</p>");
            htmlBuilder.Append("<p>" + bill.ProjectText + "</p>");
            htmlBuilder.Append("<br>");
            htmlBuilder.Append("<p>" + bill.IntroText + "</p>");
            htmlBuilder.Append("<br>");
            htmlBuilder.Append("<br>");
            htmlBuilder.Append("</div>");

            //Table Mable sag i immer
            htmlBuilder.Append("<table class=\"table table-striped\">");
            htmlBuilder.Append("<thead><tr>" +
                "<th scope=\"col\">#</th>" +
                "<th scope=\"col\">Text</th>" +
                "<th scope=\"col\">Stück</th>" +
                "<th scope=\"col\">Einheit</th>" +
                "<th scope=\"col\">Einzelpreis</th>" +
                "<th scope=\"col\">Rabatt</th>" +
                "<th scope=\"col\">Netto</th>" +
                "</tr>" +
                "</thead>"
            );
            htmlBuilder.Append("<tbody>");
            for(int i = 0; i < bill.Articles.Count; i++)
            {
                string unitPriceString = bill.Articles[i].Price.ToString("0.00");

                string priceString = (bill.Articles[i].Price * bill.Articles[i].Amount - bill.Articles[i].Price * bill.Articles[i].Amount * bill.Articles[i].Discount / 100).ToString("0.00");

                htmlBuilder.Append("<tr>");
                htmlBuilder.Append("<td>" + (i + 1) + "</td>");
                htmlBuilder.Append("<td>" + bill.Articles[i].Name + "</td>");
                htmlBuilder.Append("<td>" + bill.Articles[i].Amount + "</td>");
                htmlBuilder.Append("<td>" + bill.Articles[i].Unit + "</td>");
                htmlBuilder.Append("<td class=\"text-right\">" + unitPriceString + "€</td>");
                htmlBuilder.Append("<td>" + bill.Articles[i].Discount + " %" + "</td>");
                htmlBuilder.Append("<td class=\"text-right\">" + priceString + "€</td>");
                htmlBuilder.Append("</tr>");
            }
            htmlBuilder.Append("</tbody>");
            htmlBuilder.Append("</table>");

            //Skonto
            htmlBuilder.Append("<div>");
            htmlBuilder.Append("<p>Skonto: </p>");
            htmlBuilder.Append("<p>");
            htmlBuilder.Append(bill.CashDiscount);
            htmlBuilder.Append("/");
            htmlBuilder.Append(bill.CashDiscountLimit);
            htmlBuilder.Append("/");
            htmlBuilder.Append(bill.CashLimit);
            htmlBuilder.Append("</p>");
            htmlBuilder.Append("</div>");

            //Berechnungen
            htmlBuilder.Append("<div>");
            double nettoSum = 0;
            foreach (Article a in bill.Articles)
                nettoSum += a.Amount * a.Price * (1 - a.Discount/100);
            string nettoSumString = nettoSum.ToString("0.00");
            htmlBuilder.Append("<p class=\"text-right\">" + "Netto-Summe: " + nettoSumString + "€</p>");

            double discount = nettoSum * (bill.Discount/100);
            string discountString = discount.ToString("0.00");
            htmlBuilder.Append("<p class=\"text-right\">" + bill.Discount.ToString() + "% Rabatt: " + discountString + "€</p>");

            double nettoMinusDiscount = nettoSum - discount;
            string nettoMinusDiscountString = nettoMinusDiscount.ToString("0.00");
            htmlBuilder.Append("<p class=\"text-right\">" + "Netto abzgl. Rabatt: "+ nettoMinusDiscountString + "€</p>");

            double tax = nettoSum * (bill.Articles[0].Tax/100);
            string taxString = tax.ToString("0.00");
            htmlBuilder.Append("<p class=\"text-right\">" + "20% USt: " + taxString + "€</p>");
            //Trennlinie
            htmlBuilder.Append("<hr>");
            double finalSum = nettoMinusDiscount + tax;
            string finalSumString = finalSum.ToString("0.00");
            htmlBuilder.Append("<p class=\"text-right\">" + "Endbetrag: " + finalSumString + "€</p>");
            htmlBuilder.Append("</div>");

            //End Text
            htmlBuilder.Append("<br>");
            htmlBuilder.Append("<br>");
            htmlBuilder.Append("<br>");
            htmlBuilder.Append("<div>");
            htmlBuilder.Append("<p>");
            htmlBuilder.Append(bill.EndText);
            htmlBuilder.Append("</p>");
            htmlBuilder.Append("</div>");

            //Ending of the Body
            htmlBuilder.Append("</div>");
            htmlBuilder.Append("</body>");

            //Ending of the document
            htmlBuilder.Append("</html>");

            //Printing the Document
            HtmlToPdf pdfGenerator = new HtmlToPdf();
            pdfGenerator.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            pdfGenerator.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Portrait;
            pdfGenerator.PrintOptions.FirstPageNumber = 1;
            pdfGenerator.PrintOptions.Footer = new HtmlHeaderFooter
            {
                Height = 25,
                HtmlFragment = "<p><center>" + bill.FooterText + "</center></p><p class=\"text-right\">Seite {page} von {total-pages}</p>",
                DrawDividerLine = true,
            };

            PdfDocument doc = pdfGenerator.RenderHtmlAsPdf(htmlBuilder.ToString());
            doc.SaveAs(path);
            Process.Start(path);
        }
    }
}