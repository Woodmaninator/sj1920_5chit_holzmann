﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFGenerator
{
    public class Company
    {
        public int ID
        {
            get;set;
        }

        public string Name
        {
            get;set;
        }

        public string Street
        {
            get;set;
        }

        public int PostCode
        {
            get;set;
        }

        public string Place
        {
            get;set;
        }
    }
}
