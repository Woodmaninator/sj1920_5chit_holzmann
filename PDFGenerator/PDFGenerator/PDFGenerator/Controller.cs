﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDFGenerator
{
    public class Controller
    {
        private List<Bill> bills;
        private PDFPrinter printer;
        private DatabaseReader reader;

        public Controller()
        {
            reader = new DatabaseReader();
            printer = new PDFPrinter();
            this.bills = reader.ReadBillsFromDatabase();
        }

        public List<Bill> Bills
        {
            get => this.bills;
        }

        public void PrintPDF(Bill bill, string path)
        {
            printer.Print(bill, path);
        }
    }
}