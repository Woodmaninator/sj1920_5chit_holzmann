﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PDFGenerator;

namespace PDFGeneratorConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Controller c = new Controller();
            c.PrintPDF(c.Bills[1], "pdf.pdf");
        }
    }
}
