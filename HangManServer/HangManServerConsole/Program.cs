﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HangManServer;
using System.Threading;

namespace HangManServerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 0;
            bool success = false;
            while (!success)
            {
                Console.WriteLine("Specify Port for Server: ");
                if (Int32.TryParse(Console.ReadLine(),out port))
                {
                    success = true;
                }
                else
                {
                    Console.WriteLine("Port is not valid!");
                }
            }
            HMServer server = new HMServer(port, "words.xml");
            server.Start();
            Task task = new Task(server.AcceptConnection);
            task.Start();
            while(true)
            {
                Console.WriteLine("Enter Command: ");
                Console.WriteLine("stop");
                string command = Console.ReadLine();
                if(command == "stop")
                {
                    server.Stop();
                    break;
                }
            }
        }
    }
}
