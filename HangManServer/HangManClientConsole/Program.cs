﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HangManClient;

namespace HangManClientConsole
{
    class Program
    {
        static string currentWord;
        static int currentMistakes;
        static bool started;
        static bool gameOver;
        static async Task Main(string[] args)
        {
            bool conInputIPSuccess = false;
            bool conInputPortSuccess = false;
            IPAddress ip = IPAddress.Loopback;
            int port = 16000;
            while(!conInputIPSuccess)
            {
                Console.WriteLine("Enter IP Address");
                if (IPAddress.TryParse(Console.ReadLine(), out ip))
                    conInputIPSuccess = true;
                else
                {
                    Console.WriteLine("Invalid Input. Press enter to try again");
                    Console.ReadLine();
                }
                Console.Clear();
            }
            while(!conInputPortSuccess)
            {
                Console.WriteLine("Enter Port");
                if (Int32.TryParse(Console.ReadLine(), out port))
                    conInputPortSuccess = true;
                else
                {
                    Console.WriteLine("Invalid Input. Press enter to try again");
                    Console.ReadLine();
                }
                Console.Clear();
            }
            HMClient hmClient = new HMClient();
            hmClient.Connect(ip, port);

            hmClient.GameOver += DisplayGameOver;
            hmClient.MistakeCountUpdated += DisplayCurrentMistakesUpdated;
            hmClient.WordUpdated += DisplayWordUpdated;

            hmClient.Start();

            while (true)
            {
                if (started)
                {
                    if (gameOver)
                    {
                        started = false;
                        Console.WriteLine("Game over!");
                        gameOver = false;
                    }
                    if(currentWord != "" && currentWord.IndexOf('_') == -1 && !gameOver)
                    {
                        started = false;
                        Console.WriteLine("You win!");
                    }
                    Console.WriteLine("Current Word: " + currentWord);
                    Console.WriteLine("Current Mistakes: " + currentMistakes);
                }
                Console.WriteLine("Enter Command: ");
                if (started)
                    Console.WriteLine("guess, new");
                else
                    Console.WriteLine("new");
                switch (Console.ReadLine())
                {
                    case "guess":
                        char letter;
                        Console.WriteLine("Enter letter: ");
                        if(Char.TryParse(Console.ReadLine(), out letter))
                        {
                            await hmClient.GuessLetterAsync(letter);
                        }
                        else
                        {
                            Console.WriteLine("Invalid Input");
                            System.Threading.Thread.Sleep(1000);
                        }
                        break;
                    case "new":
                        int maxMistakes;
                        Console.WriteLine("Enter maximum mistakes: ");
                        if(Int32.TryParse(Console.ReadLine(), out maxMistakes))
                        {
                            started = true;
                            await hmClient.StartNewGameAsync(maxMistakes);
                        }
                        else
                        {
                            Console.WriteLine("Invalid Input");
                            System.Threading.Thread.Sleep(1000);
                        }
                        break;
                }
                Console.Clear();
                //System.Threading.Thread.Sleep(1000);
            }
        }
        private static void DisplayGameOver(string word)
        {
            currentWord = word;
            //Console.WriteLine("Game over! The word was " + word);
        }
        private static void DisplayWordUpdated(string word)
        {
            currentWord = word;
            //Console.WriteLine("Word updated! The current word is " + word);
        }
        private static void DisplayCurrentMistakesUpdated(int currentMistakes)
        {
            Program.currentMistakes = currentMistakes;
            //Console.WriteLine("Amount of current Mistakes: " + currentMistakes);
        }
    }
}
