﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HangManClient;
using System.Net;

namespace HangmanClientWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool gameStarted = false;
        HMClient client;
        private string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private List<Button> buttonList;
        int maxMistakes;
        public MainWindow()
        {
            InitializeComponent();
            //BUTTON LISTE INITALISIEREN
            buttonList = new List<Button>();
            //CONNECTION TO SERVER
            EnterPortDialog dialog = new EnterPortDialog();
            if (dialog.ShowDialog() == true)
            {
                try {
                    this.client = new HMClient();
                    client.GameOver += Client_GameOver;
                    client.MistakeCountUpdated += Client_MistakeCountUpdated;
                    client.WordUpdated += Client_WordUpdated;
                    client.ConnectionErrorOccured += (string message) =>
                    {
                        MessageBox.Show("A Connection Error occured. The application will now shut down\n" + message);
                        Environment.Exit(-1);
                    };
                    this.client.Connect(dialog.IpAddress, dialog.Port);
                    this.client.Start();
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message + "\nConnection failed. The application will now shut down.");
                    Environment.Exit(-1);
                }
            }
            else
            {
                MessageBox.Show("Connection failed. The application will now close");
                Environment.Exit(-1);
            }
            //BUTTON INITIALIZATION
            for (int i = 0; i < 26; i++)
            {
                char letter = this.alphabet[i];
                Button b = new Button();
                b.Width = 30;
                b.Height = 30;
                b.Margin = new Thickness(5);
                b.Content = letter.ToString();
                b.Click += LetterButton_Click;
                if(i >= 13)
                {
                    letterPanelBottom.Children.Add(b);
                }
                else
                {
                    letterPanelTop.Children.Add(b);
                }
                buttonList.Add(b);
            }
        }

        private void Client_WordUpdated(string word)
        {
            this.wordText.Dispatcher.Invoke(() =>
            {
                this.wordText.Text = word;
                if (this.wordText.Text.IndexOf('_') == -1)
                {
                    MessageBox.Show("Congratulations. You have successfully guessed the word!");
                    this.gameStarted = false;
                }
            });
        }

        private void Client_MistakeCountUpdated(int mistakeCount)
        {
            this.labelCurrentMistakes.Dispatcher.Invoke(()=> 
            {
                this.labelCurrentMistakes.Content = mistakeCount.ToString();
            });
        }

        private void Client_GameOver(string word)
        {
            this.wordText.Dispatcher.Invoke(() =>
            {
                this.wordText.Text = word;
                this.gameStarted = false;
                this.image.Visibility = Visibility.Visible;
                MessageBox.Show("Game over!");
            });
        }

        private async void LetterButton_Click(object sender, RoutedEventArgs e)
        {
            if (gameStarted)
            {
                if (sender is Button)
                {
                    Button b = (Button)sender;
                    b.IsEnabled = false;
                    char letter = Convert.ToChar(b.Content);
                    await client.GuessLetterAsync(letter);
                }
            }
            else
            {
                MessageBox.Show("Start a game before trying to guess!");
            }
        }

        private async void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            NewGameDialog dialog = new NewGameDialog();
            if(dialog.ShowDialog() == true)
            {
                int maxMistakes = dialog.MaxMistakeCount;
                this.labelCurrentMistakes.Content = "0";
                this.image.Visibility = Visibility.Hidden;
                foreach (Button b in buttonList)
                    b.IsEnabled = true;
                //START NEW GAME
                this.maxMistakes = maxMistakes;
                this.labelMaximumMistakes.Content = maxMistakes.ToString();
                this.gameStarted = true;
                await client.StartNewGameAsync(maxMistakes);
            }
        }
    }
}
