﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;

namespace HangmanClientWPF
{
    /// <summary>
    /// Interaktionslogik für EnterPortDialog.xaml
    /// </summary>
    public partial class EnterPortDialog : Window
    {
        private int port;
        private IPAddress ipAddress;

        public int Port
        {
            get { return this.port; }
        }
        public IPAddress IpAddress
        {
            get { return this.ipAddress; }
        }
        public EnterPortDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IPAddress ip;
            int port;
            if (IPAddress.TryParse(inputIP.Text, out ip) && Int32.TryParse(inputPort.Text, out port))
            {
                this.ipAddress = ip;
                this.port = port;
                this.DialogResult = true;
            }
            else
                MessageBox.Show("Invalid IP Address/Port");
        }
    }
}
