﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HangmanClientWPF
{
    /// <summary>
    /// Interaktionslogik für NewGameDialog.xaml
    /// </summary>
    public partial class NewGameDialog : Window
    {
        private int maxMistakeCount;
        public int MaxMistakeCount {
            get { return this.maxMistakeCount; }}
        public NewGameDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int value;
            if(Int32.TryParse(input.Text, out value))
            {
                this.maxMistakeCount = value;
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Please enter a valid Number!");
            }
        }
        
    }
}
