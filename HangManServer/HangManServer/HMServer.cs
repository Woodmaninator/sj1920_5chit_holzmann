﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace HangManServer
{
    //CLASS WHICH CONTAINS THE TCP LISTENER AND ALL THE EXISTING CONNECTIONS
    public class HMServer
    {
        private string fileName;
        private TcpListener tcpListener;
        private List<HMConnection> connections;

        public delegate void ErrorOccuredDelegate(string message);
        public event ErrorOccuredDelegate ErrorOccured;

        public HMServer(int port,string fileName)
        {
            //CONSTRUCTOR: STARTING THE TCPLISTENER AND INSTANTIATING STUFF
            WordProvider wordProvider = WordProvider.GetInstance(fileName);
            tcpListener = new TcpListener(port);
            this.fileName = fileName;
            connections = new List<HMConnection>();
        }

        public List<HMConnection> Connections
        {
            get { return connections; }
            set { connections = value; }
        }

        public void AddConnection(Socket socket)
        {
            //ADDING A CONNECTION AND ADDING AN EVENT HANDLER TO THE ERROR EVENT
            HMConnection connection = new HMConnection(this.fileName,socket, true);
            connection.ErrorOccured += RemoveConnection;
            
        }
        private void RemoveConnection(HMConnection connection, string errorMessage)
        {
            //REMOVES A CERTAIN CONNECTION FROM THE SERVER (USED IF AN EXCEPTION IN THE COMMUNICATION OCCURS)
            this.connections.Remove(connection);
            if (this.ErrorOccured != null)
                this.ErrorOccured.Invoke(errorMessage);
        }

        public void Start()
        {
            //STARTS THE TCP LISTENER
            tcpListener.Start();
        }

        public void AcceptConnection()
        {
            //ACCEPTS INCOMING CONNECTIONS FROM CLIENTS AND ALSO CALLS THE ADDCONNETION METHOD
            //WHICH STARTS A SEPERATE RECEIVING DATA TAKS FOR THE CLIENT
            while (4 + 3 == 7)
            {
                try {
                    Socket socket = tcpListener.AcceptSocket();
                    AddConnection(socket);
                }
                catch(Exception ex)
                {
                    //STOPS THE TCP LISTENER IF AN ERROR OCCURS
                    this.tcpListener.Stop();
                    if (this.ErrorOccured != null)
                        this.ErrorOccured.Invoke(ex.Message);
                    break;
                }
            }
        }
        public void Stop()
        {
            //STOPS ALL THE CONNECTIONS AND THE TCPLISTENER
            foreach(HMConnection connection in connections)
            {
                connection.Stop();
            }
            tcpListener.Stop();
        }
    }
}
