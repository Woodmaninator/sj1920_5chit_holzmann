﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HangManServer
{
    public class HMConnection
    {
        //CLASS WHICH HANLDES EXACTLY ONE CONNECTION (ONE CLIENT) AND CREATES A THREAD FOR THE CLIENT
        private string currentWord;
        private int mistakeCount;
        private WordProvider wordProvider;
        private int maximumMistakes;
        private Task receiveTask;
        private Socket socket;
        private Stream s;
        private StreamReader sr;
        private StreamWriter sw;
        
        private bool cancel;

        public delegate void ErrorOccuredDelegate(HMConnection connection, string errorMessage);
        public event ErrorOccuredDelegate ErrorOccured;

        public int MaximumMistakes
        {
            get { return maximumMistakes; }
            set { maximumMistakes = value; }
        }
        public string CurrentWord
        {
            get { return currentWord; }
            set { currentWord = value; }
        }

        public HMConnection(string fileName, Socket socket, bool startTask)
        {
            //CONSTRUCTOR: INSTANTIATION OF THE WORDPROVIDER AND STARTING THE RECEIVE DATA TASK
            wordProvider = WordProvider.GetInstance(fileName);
            receiveTask = new Task(ReceiveDataAsync);
            this.socket = socket;     

            if (startTask)
                StartTask();
        }

        public void StartTask()
        {
            //STARTS THE TASK AND INITIALIZES THE NETWORKSTREAM
            s = new NetworkStream(socket);
            receiveTask.Start();
        }
        private async Task SendDataAsync(string xmlData)
        {
            //WRITES THE INPUT PARAMETER TO THE NETWORK STREAM
            sw = new StreamWriter(s);
            sw.AutoFlush = true;
            await sw.WriteLineAsync(xmlData);
        }

        private async void ReceiveDataAsync()
        {
            //METHOD FOR RECEIVING DATA AND HANDLING THE DATA PROPERLY
            while (!cancel)
            {
                try
                {
                    sr = new StreamReader(s);
                    string receivedData = "";
                    StringBuilder sb = new StringBuilder();
                    while (sr.Peek() != -1)
                    {
                        sb.Append(sr.ReadLine());
                    }
                    receivedData = sb.ToString();
                    //receivedData = sr.ReadToEnd();
                    if (receivedData != "")
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(receivedData);

                        XmlNode root = doc.SelectSingleNode("HMData");
                        XmlAttribute attribute = root.Attributes["Command"];
                        string command = attribute.Value;
                        //HANDLING THE INPUT DATA ACCORDING TO THE COMMAND ATTRIBUTE OF THE HMDATA XML TAG
                        switch (command)
                        {
                            case "NEW":
                                await SendDataAsync(HandleCommandNew(doc));
                                break;
                            case "LETTER":
                                XmlNode letterNode = doc.SelectSingleNode("HMData/Letter");
                                char letter = Convert.ToChar(letterNode.InnerText);
                                letter = Char.ToUpper(letter);
                                bool found = CheckForLetterOccuranceInCurrentWord(letter);
                                List<int> occurances = GetOccurancesOfCharInCurrentWord(letter);
                                if (!found)
                                    mistakeCount++;
                                if (mistakeCount <= this.maximumMistakes)
                                {
                                    await SendDataAsync(HandleCommandReturnOccurances(occurances, found, letter));
                                }
                                else
                                {
                                    await SendDataAsync(HandleCommandGameOver());
                                }
                                break;
                        }
                    }
                }
                catch(Exception ex)
                {
                    this.cancel = true;
                    if (this.ErrorOccured != null)
                        this.ErrorOccured.Invoke(this, ex.Message);
                }
            }
        }
        public string HandleCommandNew(XmlDocument doc)
        {
            //RETURNS THE XML STRING OF THE NEW COMMAND
            XmlNode maximumMistakes = doc.SelectSingleNode("HMData/MaximumMistakes");
            ResetGame(Convert.ToInt32(maximumMistakes.InnerText));
            XmlDocument sendDoc = new XmlDocument();
            sendDoc.LoadXml("<HMData Command=\"NEW\"><Length>" + this.currentWord.Length + "</Length></HMData>");
            XmlNode lengthNode = sendDoc.SelectSingleNode("HMData/Length");
            lengthNode.InnerText = currentWord.Length.ToString();
            return sendDoc.OuterXml;
        }
        public string HandleCommandGameOver()
        {
            //RETURNS THE XML STRING OF THE GAME OVER COMMAND
            XmlDocument sendLetterDoc = new XmlDocument();
            sendLetterDoc.LoadXml("<HMData Command=\"GAMEOVER\"><Word>" + currentWord + "</Word></HMData>");
            return sendLetterDoc.OuterXml;
        }
        public string HandleCommandReturnOccurances(List<int> occurances, bool found, char letter)
        {
            //RETURNS THE XML STRING OF THE COMMAND WHICH RETURNS THE OCCURANCES OF A LETTER IN THE CURRENT WORD
            XmlDocument sendLetterDoc = new XmlDocument();
            sendLetterDoc.LoadXml("<HMData Command=\"LETTER\"><Letter>" + letter + "</Letter><Success></Success><CurrentMistakes></CurrentMistakes><Positions></Positions></HMData>");
            sendLetterDoc.SelectSingleNode("HMData/CurrentMistakes").InnerText = mistakeCount.ToString();
            if (found)
                sendLetterDoc.SelectSingleNode("HMData/Success").InnerText = "TRUE";
            else
                sendLetterDoc.SelectSingleNode("HMData/Success").InnerText = "FALSE";

            foreach (int number in occurances)
            {
                XmlNode position = sendLetterDoc.CreateNode(XmlNodeType.Element, "Position", null);
                position.InnerText = number.ToString();
                sendLetterDoc.SelectSingleNode("HMData/Positions").AppendChild(position);
            }
            return sendLetterDoc.OuterXml;
        }
        public bool CheckForLetterOccuranceInCurrentWord(char letter)
        {
            //RETURNS WHETHER A CHAR IS IN THE CURRENT WORD OR NOT
            if (currentWord.IndexOf(letter) != -1)
            {
                return true;
            }
            return false;
        }
        public List<int> GetOccurancesOfCharInCurrentWord(char letter)
        {
            //RETURNS ALL THE POSITIONS OF A CHAR IN THE CURRENT WORD
            List<int> occurences = new List<int>();
            if (currentWord.IndexOf(letter) != -1)
            {
                int currentIndex = 0;
                while (currentWord.IndexOf(letter, currentIndex) != -1)
                {
                    occurences.Add(currentWord.IndexOf(letter, currentIndex));
                    currentIndex++;
                }
            }
            return occurences;
        }
        public void ResetGame(int maximumMistakes)
        {
            //RESETS THE MAXIMUM MISTAKES, LOADS A RANDOM WORD AND RESETS THE MISTAKE COUNTER
            this.maximumMistakes = maximumMistakes;
            currentWord = wordProvider.GetRandomWord();
            mistakeCount = 0;
        }
        public void Stop()
        {
            //STOPS THE CONNECTION/COMMUNICATION
            if (receiveTask.Status == TaskStatus.Running)
                this.cancel = true;
            sw.Close();
            sr.Close();
            s.Close();
        }
    }
}