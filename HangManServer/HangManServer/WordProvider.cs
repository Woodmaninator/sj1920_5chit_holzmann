﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;

namespace HangManServer
{
    public class WordProvider
    {
        //CLASS RESPONSIBLE FOR READING THE WORDS FROM A FILE AND THEN SELECTING A RANDOM WORD
        private static WordProvider instance;
        private string fileName;
        private List<string> words;

        public List<string> Words
        {
            get { return words; }
        }
        protected WordProvider(string fileName)
        {
            //CONSTRUCTOR: CALLS THE READWORDS METHOD WHICH READS THE WORDS FROM A FILE
            this.fileName = fileName;
            words = new List<string>();
            ReadWords();
            //IF THERE ARE NO WORDS IN THE FILE, DEFAULT WORD IS LOADED TO PROVIDE THE APPLICATION WITH A WORD
            if (words.Count == 0)
                words.Add("EMPTY");
        }

        public static WordProvider GetInstance(string fileName)
        {
            //CREATES A NEW INSTANCE IF THE INSTANCE IS NULL
            //AND RETURNS THE INSTANCE
            if(instance == null)
            {
                instance = new WordProvider(fileName);
            }
            return instance;
        }

        public string GetRandomWord()
        {
            //CHOSES A RANDOM WORD FROM THE LIST
            Random r = new Random();
            return words[r.Next(0, words.Count)];
        }

        private void ReadWords()
        {
            //READS ALL WORDS FROM THE CHOSEN FILE
            //REGEX CHECKS WHETER THE WORD CONTAINS ANYTHING BUT A-Z
            //IF THE WORD CONTAINS UNALLOWED CHARACTERS THE WORD IS DISCARDED
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                XmlNodeList hmWords = xmlDocument.SelectNodes("HMWords/Word");
                Regex regex = new Regex("^[A-Za-z]+$");
                foreach (XmlNode node in hmWords)
                {
                    string word = node.InnerText;
                    if (regex.Match(word).Success)
                        words.Add(node.InnerText.ToUpper());
                }
            }
            catch(Exception ex)
            {
                //IF AN EXCEPTION OCCURS, THE WORDLIST WILL CONTAIN ONLY ONE WORD
                words = new List<string>();
                words.Add("EXCEPTION");
            }
        }
    }
}