﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HangManClient;
using HangManServer;
using System.Xml;
using System.Collections.Generic;
using System.Net.Sockets;

namespace HangManUnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestLoadWordsMethod()
        {
            WordProvider wordProvider = WordProvider.GetInstance("words.xml");
            int expectedSize = 1;
            int actualSize = wordProvider.Words.Count;

            Assert.AreEqual(expectedSize, actualSize);
        }
        [TestMethod]
        public void TestGetWord()
        {
            WordProvider wordProvider = WordProvider.GetInstance("words.xml");
            string expectedWord = "APFEL";
            string actualWord = wordProvider.GetRandomWord();

            Assert.AreEqual(expectedWord, actualWord);
        }
        [TestMethod]
        public void TestClientCreateNewGameXML()
        {
            HMClient client = new HMClient();
            string expected = "<HMData Command=\"NEW\"><MaximumMistakes>5</MaximumMistakes></HMData>";
            string actual = client.CreateStartNewGameXML(5);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestClientCreateGuessLetterXML()
        {
            HMClient client = new HMClient();
            string expected = "<HMData Command=\"LETTER\"><Letter>K</Letter></HMData>";
            string actual = client.CreateGuessLetterXML('K');

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestClientGetLetterFromXML()
        {
            HMClient client = new HMClient();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command =\"LETTER\"><Letter>K</Letter><Success>TRUE</Success><CurrentMistakes>4</CurrentMistakes><Positions><Position>2</Position><Position>4</Position></Positions></HMData>");

            char expected = 'K';
            char actual = client.GetLetterFromXML(doc);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestClientGetSuccessFromXMLExpectsTrue()
        {
            HMClient client = new HMClient();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command =\"LETTER\"><Letter>K</Letter><Success>TRUE</Success><CurrentMistakes>4</CurrentMistakes><Positions><Position>2</Position><Position>4</Position></Positions></HMData>");

            bool expected = true;
            bool actual = client.GetSuccessFromXML(doc);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestClientGetSuccessFromXMLExpectsFalse()
        {
            HMClient client = new HMClient();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command =\"LETTER\"><Letter>K</Letter><Success>FALSE</Success><CurrentMistakes>4</CurrentMistakes><Positions></Positions></HMData>");

            bool expected = false;
            bool actual = client.GetSuccessFromXML(doc);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestClientGetCurrentMistakeCountFromXML()
        {
            HMClient client = new HMClient();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command =\"LETTER\"><Letter>K</Letter><Success>TRUE</Success><CurrentMistakes>4</CurrentMistakes><Positions><Position>2</Position><Position>4</Position></Positions></HMData>");

            int expected = 4;
            int actual = client.GetCurrentMistakeCountFromXML(doc);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestClientGetWordFromSuccessPositions()
        {
            HMClient client = new HMClient();
            client.CurrentWord = "_ _ _ _ _";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command =\"LETTER\"><Letter>K</Letter><Success>TRUE</Success><CurrentMistakes>4</CurrentMistakes><Positions><Position>2</Position><Position>4</Position></Positions></HMData>");

            string expected = "_ _ K _ K";
            string actual = client.GetWordFromSuccessPositions(doc, 'K');

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestServerHandleCommandNew()
        {
            HMConnection con = new HMConnection("words.xml", null, false);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command=\"NEW\"><MaximumMistakes>5</MaximumMistakes></HMData>");

            string expected = "<HMData Command=\"NEW\"><Length>5</Length></HMData>";
            string actual = con.HandleCommandNew(doc);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestServerHandleCommandGameOver()
        {
            HMConnection con = new HMConnection("words.xml", null, false);
            con.CurrentWord = "APFEL";

            string expected = "<HMData Command=\"GAMEOVER\"><Word>APFEL</Word></HMData>";
            string actual = con.HandleCommandGameOver();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestServerHandleCommandReturnOccurances()
        {
            HMConnection con = new HMConnection("words.xml", null, false);

            string expected = "<HMData Command=\"LETTER\"><Letter>K</Letter><Success>TRUE</Success><CurrentMistakes>0</CurrentMistakes><Positions><Position>2</Position><Position>4</Position></Positions></HMData>";
            string actual = con.HandleCommandReturnOccurances(new List<int> { 2,4}, true, 'K');

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestServerCheckForLetterOccuranceInCurrentWordExpectsFalse()
        {
            HMConnection con = new HMConnection("words.xml", null, false);
            con.CurrentWord = "APFEL";

            bool expected = false;
            bool actual = con.CheckForLetterOccuranceInCurrentWord('K');

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestServerCheckForLetterOccuranceInCurrentWordExpectsTrue()
        {
            HMConnection con = new HMConnection("words.xml", null, false);
            con.CurrentWord = "APFEL";

            bool expected = true;
            bool actual = con.CheckForLetterOccuranceInCurrentWord('A');

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestServerGetOccurancesOfCharInCurrentWord()
        {
            HMConnection con = new HMConnection("words.xml", null, false);
            con.CurrentWord = "APFEL";

            List<int> expected = new List<int> { 0 };
            List<int> actual = con.GetOccurancesOfCharInCurrentWord('A');

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
