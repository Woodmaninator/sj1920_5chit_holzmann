﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Xml;

namespace HangManClient
{
    public class HMClient
    {
        //CLASS WHICH HANDLES THE CONNECTION AND COMMUNICATION TO THE SERVER
        private Task receiveTask;

        private System.Net.Sockets.TcpClient client;
        private NetworkStream s;
        private StreamReader sr;
        private StreamWriter sw;
        private string currentWord;
        private int currentMistakeCount;

        private bool cancel;

        public string CurrentWord { get { return this.currentWord; } set { this.currentWord = value; } }

        //EVENT FOR UPDATING THE MISTAKE COUNT
        public delegate void MistakeCountUpdatedDelegate(int mistakeCount);
        public event MistakeCountUpdatedDelegate MistakeCountUpdated;

        //EVENT FOR UPDATING THE WORD
        public delegate void WordUpdatedDelegate(string word);
        public event WordUpdatedDelegate WordUpdated;

        //EVENT FOR TELLING THE USER THE GAME IS OVER
        public delegate void GameOverDelegate(string word);
        public event GameOverDelegate GameOver;

        //EVENT FOR TELLING THE USER AN ERROR OCCURED
        public delegate void ConnectionErrorOccuredDelegate(string errorMessage);
        public event ConnectionErrorOccuredDelegate ConnectionErrorOccured;

        public HMClient()
        {
            client = new TcpClient();
        }

        public void Connect(IPAddress ip,int port)
        {
            //CONNECTS THE CLIENT TO THE SERVER
            try
            {
                if (!client.Connected)
                    client.Connect(ip, port);
                s = client.GetStream();
            }
            catch (Exception ex)
            {
                //TELLS THE USER THE CONNECTION DID NOT SUCCEED
                if (ConnectionErrorOccured != null)
                    ConnectionErrorOccured.Invoke(ex.Message);
            }
            //CREATES THE TAKS FOR HANDLING THE INCOMING DATA
            receiveTask = new Task(ReceiveDataAsync);
        }
        public void Start()
        {
            //STARTS THE RECEIVE DATA TASK
            if (receiveTask.Status != TaskStatus.Running)
                receiveTask.Start();
        }

        private void ReceiveDataAsync()
        {
            //HANDLES THE INCOMING DATA
            while (!cancel)
            {
                try
                {
                    string receivedData = "";
                    StringBuilder sb = new StringBuilder();

                    sr = new StreamReader(s);

                    while (sr.Peek() != -1)
                    {
                        sb.Append(sr.ReadLine());
                    }
                    receivedData = sb.ToString();
                    //receivedData = sr.ReadToEnd();
                    if (receivedData != "")
                    {
                        XmlDocument doc = new XmlDocument();
                        try
                        { doc.LoadXml(receivedData); }
                        catch
                        { continue; }
                        XmlNode root = doc.SelectSingleNode("HMData");
                        XmlAttribute attribute = root.Attributes["Command"];
                        string command = attribute.Value;
                        //HANDLES THE INCOMING INPUT DATA ACCORDING TO THE COMMAND ATTRIBUTE OF THE HMDATA TAG
                        switch (command)
                        {
                            case "NEW":
                                //BUILDS THE NEW WORD TEMPLATE ACCORDING TO THE LENGTH OF THE WORD
                                XmlNode wordLength = doc.SelectSingleNode("HMData/Length");
                                StringBuilder wordBuilder = new StringBuilder();
                                for (int i = 0; i < Convert.ToInt32(wordLength.InnerText); i++)
                                {
                                    wordBuilder.Append("_ ");
                                }
                                this.currentWord = wordBuilder.ToString();
                                this.currentMistakeCount = 0;
                                //INVOKES THE WORDUPDATED EVENT WITH THE NEW WORD
                                //INVOKES THE MISTAKECOUNT UPDATED EVENT AND RESETS THE MISTAKE COUNT
                                if (this.WordUpdated != null)
                                    this.WordUpdated.Invoke(this.currentWord);
                                if (this.MistakeCountUpdated != null)
                                    this.MistakeCountUpdated.Invoke(this.currentMistakeCount);
                                break;
                            case "LETTER":
                                //VARIABLE WITH THE LETTER THE USER HAS GUESSED
                                char letter = GetLetterFromXML(doc);
                                //VARIABLE FOR WHETHER THE LETTER IS IN THE WORD OR NOT
                                bool success = GetSuccessFromXML(doc);
                                this.currentMistakeCount = GetCurrentMistakeCountFromXML(doc);
                                //UPDATES THE CURRENT WORD BASED ON THE SUCCESSFUL POSITONS
                                this.currentWord = GetWordFromSuccessPositions(doc, letter);
                                //MISTAKE COUNT AND THE WORD IS BEING UPDATED TO LET THER USER KNOW THEY HAVE CHANGED
                                if (MistakeCountUpdated != null)
                                    this.MistakeCountUpdated(this.currentMistakeCount);
                                if (WordUpdated != null)
                                    this.WordUpdated(this.currentWord);
                                break;
                            case "GAMEOVER":
                                //INVOKES THE GAME OVER EVENT AND TELLS THE USER THE WORD HE WAS NOT ABLE TO GUESS
                                XmlNode wordNode = doc.SelectSingleNode("HMData/Word");
                                if (this.GameOver != null)
                                    this.GameOver.Invoke(wordNode.InnerText);
                                break;
                        }
                    }
                }
                catch(Exception ex)
                {
                    //CATCH BLOCK FOR WHEN READING DATA FROM THE NETWORK STREAM GOES WRONG
                    if (this.ConnectionErrorOccured != null)
                        ConnectionErrorOccured.Invoke(ex.Message);
                }
            }
        }

        public async Task SendDataAsync(string xmlData)
        {
            try
            {
                //SENDS DATA TO THE NETWORK STREAM
                sw = new StreamWriter(s);
                sw.AutoFlush = true;
                await sw.WriteLineAsync(xmlData);
            }
            catch (Exception ex)
            {
                if (this.ConnectionErrorOccured != null)
                    ConnectionErrorOccured.Invoke(ex.Message);
            }
        }
        public async Task StartNewGameAsync(int maxMistakes)
        {
            //SENDS THE SERVER THE XML FILE FOR STARTING A NEW GAME
            
            await SendDataAsync(CreateStartNewGameXML(maxMistakes));
        }
        public string CreateStartNewGameXML(int maxMistakes)
        {
            //CREATES THE XML STRING FOR THE NEW GAME COMMAND
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command=\"NEW\"><MaximumMistakes>" + maxMistakes + "</MaximumMistakes></HMData>");
            return doc.OuterXml;
        }
        public async Task GuessLetterAsync(char letter)
        {
            //SENDS THE GUESS LETTER XML TO THE SERVER
            await SendDataAsync(CreateGuessLetterXML(letter));   
        }
        public string CreateGuessLetterXML(char letter)
        {
            //CREATES THE XML STRING FOR THE GUESS LETTER COMMAND
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<HMData Command=\"LETTER\"><Letter>" + letter + "</Letter></HMData>");
            return doc.OuterXml;
        }
        public char GetLetterFromXML(XmlDocument doc)
        {
            //RETURNS THE VALUE OF THE LETTER TAG FROM AN XML FILE
            char letter;
            XmlNode letterNode = doc.SelectSingleNode("HMData/Letter");
            letter = Convert.ToChar(letterNode.InnerText);
            return letter;
        }
        public int GetCurrentMistakeCountFromXML(XmlDocument doc)
        {
            //RETURNS THE VALUE OF THE CURRENTMISTAKES TAG FROM AN XML FILE
            int currentMistakes = 0;

            XmlNode receivedMistakesNode = doc.SelectSingleNode("HMData/CurrentMistakes");
            currentMistakes = Convert.ToInt32(receivedMistakesNode.InnerText);

            return currentMistakes;
        }
        public bool GetSuccessFromXML(XmlDocument doc)
        {
            //RETURNS THE VALUE OF THE SUCCESS TAG FROM AN XML FILE
            bool success;
            XmlNode successNode = doc.SelectSingleNode("HMData/Success");
            success = Convert.ToBoolean(successNode.InnerText.ToLower());
            return success;
        }
        public string GetWordFromSuccessPositions(XmlDocument doc, char letter)
        {
            //RETURNS THE UPDATED WORD BASED ON THE POSITIONS TAG FROM THE XML DOCUMENT AND THE SELECTED LETTER
            string newWord = this.currentWord;

            XmlNodeList xmlNodeList = doc.SelectNodes("HMData/Positions/Position");
            foreach (XmlNode positionNode in xmlNodeList)
            {
                //REPLACES THE UNDERSCORE IN THE CURRENT WORD WITH THE ACTUAL LETTER IN THE CORRECT POSITION
                char[] charArr = newWord.ToCharArray();
                charArr[Convert.ToInt32(positionNode.InnerText) * 2] = letter;
                newWord = new string(charArr);
            }

            return newWord;
        }
        public void CloseConnection()
        {
            //CLOSES ALL THE EXISTING STREAMS AND CANCELS THE RECEIVING TASK
            this.cancel = true;
            if (sr != null)
            {
                sr.Close();
            }
            if (sw != null)
            {
                sw.Close();
            }
            if (s != null)
            {
                s.Close();
            }
        }
    }
}