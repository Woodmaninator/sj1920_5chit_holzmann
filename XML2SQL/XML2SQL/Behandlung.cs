﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML2SQL
{
    class Behandlung
    {
        private int id;
        private string name;
        public Behandlung(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
        public int Id
        {
            get { return this.id; }
        }
        public String Name
        {
            get { return this.name; }
        }
    }
}
