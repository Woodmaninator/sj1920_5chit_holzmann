﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XML2SQL
{
    class PatientBehandlungController
    {
        private List<Patient> patienten;
        private List<Behandlung> behandlungen;
        private List<PatientBehandlung> patientBehandlungen;
        public PatientBehandlungController(string xmlFileName)
        {
            patienten = new List<Patient>();
            behandlungen = new List<Behandlung>();
            patientBehandlungen = new List<PatientBehandlung>();
            LoadXMLFile(xmlFileName);
        }
        private void LoadXMLFile(string xmlFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFileName);

            int currentBehandlungId = 1;

            XmlNodeList patientenNodes = doc.SelectNodes("/Liste/Name");

            foreach (XmlNode patientNode in patientenNodes)
            {
                int id = Convert.ToInt32(patientNode.Attributes["ID"].Value);
                string vorname = patientNode.SelectSingleNode("Vorname").InnerText;
                string nachname = patientNode.SelectSingleNode("Nachname").InnerText;
                string svnr = patientNode.SelectSingleNode("SV").InnerText;

                Patient patient = new Patient(id, vorname, nachname, svnr);

                XmlNodeList behandlungenNodes = patientNode.SelectNodes("Behandlungen/Behandlung");

                foreach(XmlNode behandlungNode in behandlungenNodes)
                {
                    if(!behandlungen.Exists(x=> x.Name == behandlungNode.SelectSingleNode("Bezeichnung").InnerText))
                    {
                        int bId = currentBehandlungId;
                        currentBehandlungId++;
                        string bName = behandlungNode.SelectSingleNode("Bezeichnung").InnerText;
                        Behandlung b = new Behandlung(bId, bName);
                        behandlungen.Add(b);
                    }

                    Behandlung currentBehandlung = behandlungen.Find(x => x.Name == behandlungNode.SelectSingleNode("Bezeichnung").InnerText);

                    DateTime date = DateTime.Parse(behandlungNode.SelectSingleNode("Datum").InnerText);
                    int duration = Int32.Parse(behandlungNode.SelectSingleNode("Dauer").InnerText);

                    PatientBehandlung patientBehandlung = new PatientBehandlung(currentBehandlung, patient, date, duration);
                    patientBehandlungen.Add(patientBehandlung);
                }

                patienten.Add(patient);
            }
        }
        public void ClearDatabase()
        {
            OleDbConnection con = null;
            try
            {
                string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Transfer.accdb; Persist Security Info= False;";
                con = new OleDbConnection(connectionString);
                con.Open();

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                cmd.CommandText = "DELETE FROM PatientBehandlungen";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "DELETE FROM Patient";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "DELETE FROM Behandlung";
                cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con != null)
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
            }
        }
        public void TransferXMLToSQL()
        {
            OleDbConnection con = null;
            try
            {
                string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Transfer.accdb; Persist Security Info= False;";
                con = new OleDbConnection(connectionString);
                con.Open();

                foreach (Patient p in patienten)
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "INSERT INTO Patient (ID, Nachname, Vorname, SVNR) VALUES (" + p.Id + ",'" + p.Nachname + "','" + p.Vorname + "','" + p.Svnr + "')";
                    cmd.ExecuteNonQuery();
                }
                foreach (Behandlung b in behandlungen)
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "INSERT INTO Behandlung (ID, Bezeichnung) VALUES (" + b.Id + ",'" + b.Name + "')";
                    cmd.ExecuteNonQuery();
                }
                foreach (PatientBehandlung pb in patientBehandlungen)
                {
                    string formatedDate = pb.Date.Day + "." + pb.Date.Month + "." + pb.Date.Year;
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "INSERT INTO PatientBehandlungen (PatientID, BehandlungsID, Dauer, Datum) VALUES (" + pb.Patient.Id + "," + pb.Behandlung.Id + "," + pb.Duration + ",'" + formatedDate + "')";
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con != null)
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
            }
        }
        public int GetAnzahlPatienten()
        {
            return this.patienten.Count;
        }
        public int GetAnzahlPatientBehandlungen()
        {
            return this.patientBehandlungen.Count;
        }
        public int GetAnzahlBehandlungen()
        {
            return this.behandlungen.Count;
        }
        public int GetPatientenWithOneBehandlung()
        {
            return patienten.FindAll(x => {
                return patientBehandlungen.FindAll(y => y.Patient.Id == x.Id).Count == 1;
            }).Count;
        }
        public int GetPatientenWithTwoBehandlungen()
        {
            return patienten.FindAll(x => {
                return patientBehandlungen.FindAll(y => y.Patient.Id == x.Id).Count == 2;
            }).Count;
        }
        public int GetPatientenWithMoreThanTwoBehandlungen()
        {
            return patienten.FindAll(x => {
                return patientBehandlungen.FindAll(y => y.Patient.Id == x.Id).Count > 2;
            }).Count;
        }
        public int GetBehandlungDurationSum()
        {
            int sum = 0;
            foreach (PatientBehandlung pb in patientBehandlungen)
                sum += pb.Duration;
            return sum;
        }
        public int GetAnzahlPatientenSQL()
        {
            return ExecuteSQLScalar("Select Count(id) from Patient");
        }
        public int GetAnzahlPatientBehandlungenSQL()
        {
            return ExecuteSQLScalar("Select Count(PatientID) from PatientBehandlungen");
        }
        public int GetAnzahlBehandlungenSQL()
        {
            return ExecuteSQLScalar("Select Count(id) from Behandlung");
        }
        public int GetPatientenWithOneBehandlungSQL()
        {
            return ExecuteSQLScalar("SELECT COUNT(ID) FROM (SELECT COUNT(BehandlungsID) as c, PatientID as ID FROM PatientBehandlungen GROUP BY PatientID) WHERE c = 1;");
        }
        public int GetPatientenWithTwoBehandlungenSQL()
        {
            return ExecuteSQLScalar("SELECT COUNT(ID) FROM (SELECT COUNT(BehandlungsID) as c, PatientID as ID FROM PatientBehandlungen GROUP BY PatientID) WHERE c = 2;");
        }
        public int GetPatientenWithMoreThanTwoBehandlungenSQL()
        {
            return ExecuteSQLScalar("SELECT COUNT(ID) FROM(SELECT COUNT(BehandlungsID) as c, PatientID as ID FROM PatientBehandlungen GROUP BY PatientID) WHERE c > 2; ");
        }
        public int GetBehandlungDurationSumSQL()
        {
            return ExecuteSQLScalar("SELECT SUM(Dauer) FROM PatientBehandlungen;");
        }
        private int ExecuteSQLScalar(string query)
        {
            int erg;
            OleDbConnection con = null;
            try
            {
                string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Transfer.accdb; Persist Security Info= False;";
                con = new OleDbConnection(connectionString);
                con.Open();

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                cmd.CommandText = query;
                object x = cmd.ExecuteScalar();
                if (x is double)
                    erg = Convert.ToInt32((double) x);
                else
                    erg = (int)x;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (con != null)
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
            }
            return erg;
        }
    }
}
