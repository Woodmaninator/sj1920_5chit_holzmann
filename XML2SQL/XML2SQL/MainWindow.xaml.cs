﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace XML2SQL
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PatientBehandlungController controller;
        public MainWindow()
        {
            InitializeComponent();
            transferButton.IsEnabled = false;
        }
        private void SelectFileButton_Click(object sender, RoutedEventArgs e)
        {
            string dialogFilter = "XML Files (*.xml)|*.xml";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = dialogFilter;
            openFileDialog.Multiselect = false;
            if(openFileDialog.ShowDialog() == true)
            {
                string path = openFileDialog.FileName;
                transferButton.IsEnabled = true;
                controller = new PatientBehandlungController(path);
                labelSelectedFile.Content = "Selected File: " + path;
                LoadKeyValuesXML();
            }
            else
            {
                MessageBox.Show("File Selection Error!");
                transferButton.IsEnabled = false;
            }
        }

        private void TransferButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                controller.ClearDatabase();
                controller.TransferXMLToSQL();
                LoadKeyValuesSQL();
                CheckValues();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Transfer failed!\n"+ex.Message);
            }
        }
        private void LoadKeyValuesXML()
        {
            labelXML1.Content = controller.GetAnzahlPatienten();
            labelXML2.Content = controller.GetAnzahlBehandlungen();
            labelXML3.Content = controller.GetAnzahlPatientBehandlungen();
            labelXML4.Content = controller.GetPatientenWithOneBehandlung();
            labelXML5.Content = controller.GetPatientenWithTwoBehandlungen();
            labelXML6.Content = controller.GetPatientenWithMoreThanTwoBehandlungen();
            labelXML7.Content = controller.GetBehandlungDurationSum();
        }
        private void LoadKeyValuesSQL()
        {
            labelSQL1.Content = controller.GetAnzahlPatientenSQL();
            labelSQL2.Content = controller.GetAnzahlBehandlungenSQL();
            labelSQL3.Content = controller.GetAnzahlPatientBehandlungenSQL();
            labelSQL4.Content = controller.GetPatientenWithOneBehandlungSQL();
            labelSQL5.Content = controller.GetPatientenWithTwoBehandlungenSQL();
            labelSQL6.Content = controller.GetPatientenWithMoreThanTwoBehandlungenSQL();
            labelSQL7.Content = controller.GetBehandlungDurationSumSQL();
        }
        private void CheckValues()
        {
            bool success = true;
            if (labelXML1.Content.ToString() != labelSQL1.Content.ToString())
                success = false;
            if (labelXML2.Content.ToString() != labelSQL2.Content.ToString())
                success = false;
            if (labelXML3.Content.ToString() != labelSQL3.Content.ToString())
                success = false;
            if (labelXML4.Content.ToString() != labelSQL4.Content.ToString())
                success = false;
            if (labelXML5.Content.ToString() != labelSQL5.Content.ToString())
                success = false;
            if (labelXML6.Content.ToString() != labelSQL6.Content.ToString())
                success = false;
            if (labelXML7.Content.ToString() != labelSQL7.Content.ToString())
                success = false;


            if (success == true)
                MessageBox.Show("Transfer successful");
            else
                MessageBox.Show("Transfer failed");
        }
    }
}
