﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML2SQL
{
    class Patient
    {
        private int id;
        private string vorname;
        private string nachname;
        private string svnr;

        public Patient(int id, string vorname, string nachname, string svnr)
        {
            this.id = id;
            this.vorname = vorname;
            this.nachname = nachname;
            this.svnr = svnr;
        }
        public int Id
        {
            get { return this.id; }
        }
        public string Vorname
        {
            get { return this.vorname; }
        }
        public string Nachname
        {
            get { return this.nachname; }
        }
        public string Svnr
        {
            get { return this.svnr; }
        }
    }
}
