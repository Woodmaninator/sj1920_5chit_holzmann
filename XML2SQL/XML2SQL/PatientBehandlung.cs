﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML2SQL
{
    class PatientBehandlung
    {
        private Behandlung behandlung;
        private Patient patient;
        private DateTime date;
        private int duration;

        public PatientBehandlung(Behandlung behandlung, Patient patient, DateTime date, int duration)
        {
            this.behandlung = behandlung;
            this.patient = patient;
            this.date = date;
            this.duration = duration;
        }
        public Behandlung Behandlung
        {
            get { return this.behandlung; }
        }
        public Patient Patient
        {
            get { return this.patient; }
        }
        public DateTime Date
        {
            get { return this.date; }
        }
        public int Duration
        {
            get { return this.duration; }
        }
    }
}
